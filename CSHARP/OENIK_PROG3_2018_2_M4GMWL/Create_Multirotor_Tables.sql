﻿CREATE TABLE brands
(
id int primary key,
name varchar(200),
country varchar(200)
)

CREATE TABLE frames
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
size int,
psize int,
msize varchar(200),
armpiece bit,
armthickness int,
bplatethickness int,
tplatetickness int,
mass int,
material varchar(200)
)

CREATE TABLE motors
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
size int,
kv int,
ampdraw int,
thrust int,
mass int,
cells varchar(10)
)

CREATE TABLE FC
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
esc bit,
pdb bit,
vtx bit,
osd bit,
compass bit,
baro bit,
bbox bit,
cpu varchar(50),
os varchar(200),
mass int
)

CREATE TABLE ESC
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
fc_id int references FC(id),
cells varchar(10),
amp int,
burst varchar(20),
bamp int,
mass int
)

CREATE TABLE PDB
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
fc_id int references FC(id),
amp int,
bec5 bit,
bec12 bit,
motors int,
mass int
)

CREATE TABLE Cams
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
res varchar(200),
pal bit,
ntsc bit,
mass int
)

CREATE TABLE VTX
(
id int primary key,
name varchar(200),
fc_id int references FC(id),
brand_id int references brands(id),
channels int,
freq varchar(20),
tpower varchar(200),
mass int
)

CREATE TABLE prop
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
size varchar(50),
mass int
)

CREATE TABLE battery
(
id int primary key,
name varchar(200),
brand_id int references brands(id),
cells int,
c int,
mass int
)

CREATE TABLE builds
(
build_id int primary key,
buildname varchar(200),
brand_id int references brands(id),
frame_id int references frames(id),
motor_id int references motors(id),
esc_id int references ESC(id),
pdb_id int references PDB(id),
fc_id int references FC(id),
camera_id int references Cams(id),
vtx_id int references VTX(id),
prop_id int references prop(id),
battery_id int references battery(id)
)