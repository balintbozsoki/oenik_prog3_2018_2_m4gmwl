﻿// <copyright file="GetTheJoke.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Gets the joke from the java server so you can get it too.
    /// </summary>
    public static class GetTheJoke
    {
        /// <summary>
        /// Get a joke from the java server.
        /// </summary>
        public static void GetOne()
        {
            using (WebClient wc = new WebClient())
            {
                List<string> joke = new List<string>();
                joke.Add(JValue.Parse(wc.DownloadString("http://localhost:8084/OENIK_PROG3_2018_2_M4GMWL/JsonSenderServlet")).ToString(Formatting.Indented));
                ParamCollector.QueryResult.Clear();
                ParamCollector.QueryResult.Add(joke);
            }
        }
    }
}
