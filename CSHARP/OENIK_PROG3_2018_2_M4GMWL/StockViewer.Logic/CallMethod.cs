﻿// <copyright file="CallMethod.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Data;
    using StockViewer.Repository;

    /// <summary>
    /// Calls a method based on the text it gets.
    /// </summary>
    public static class CallMethod
    {
        /// <summary>
        /// Calls a method based on a string command.
        /// </summary>
        /// <param name="command">String command from the console (DisplayMenuItems).</param>
        /// <returns>Returns false if the called method had a problem but didn't throw an exception.</returns>
        public static bool Call(string command)
        {
            using (MultirotorDBEntities db = new MultirotorDBEntities())
            {
                using (QueryAllTables queryAll = new QueryAllTables(db))
                {
                    ComplexOperations complex = new ComplexOperations();
                    switch (command)
                    {
                        case "Close console":
                            Environment.Exit(0);
                            break;
                        case "Get_build_mass":
                            complex.Get_Build_Mass(queryAll.Builds.Result);
                            break;
                        case "List_build_TWR":
                            complex.List_Build_Twr(queryAll.Builds.Result);
                            break;
                        case "List_brand_products":
                            complex.List_Brand_Products(queryAll.Brands.Result);
                            break;
                        case "joke":
                            GetTheJoke.GetOne();
                            break;
                    }
                }
            }

            if (ParamCollector.Find("Table") != null && ParamCollector.Find("Table").Value != null)
            {
                int id = 1;
                using (MultirotorDBEntities db = new MultirotorDBEntities())
                {
                    if (ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                    {
                        id = int.Parse(ParamCollector.Find("ID").Value.ToString());
                    }
                    else if (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null)
                    {
                        id = int.Parse(ParamCollector.Find("id").Value.ToString());
                    }
                    else if (ParamCollector.Find("build_id") != null && ParamCollector.Find("build_id").Value != null)
                    {
                        id = int.Parse(ParamCollector.Find("build_id").Value.ToString());
                    }

                    string tablename = ParamCollector.Find("Table").Value.ToString();
                    switch (tablename)
                    {
                        case "frames":
                            Expression<Func<frames, bool>> framewhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                framewhereCondition = x => x.id == id;
                            }
                            else
                            {
                                framewhereCondition = null;
                            }

                            frames newframe = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newframe = CreateNewRecord.NewFrame();
                            }

                            using (Operations<frames> frame_operation = new Operations<frames>(command, framewhereCondition, newframe, db))
                            {
                                IQueryable<frames> framesResult = frame_operation.Result;
                                ParamCollector.QueryResult.Clear();
                                if (framesResult != null)
                                {
                                    foreach (var item in framesResult)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "battery":
                            Expression<Func<battery, bool>> batterywhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                batterywhereCondition = x => x.id == id;
                            }
                            else
                            {
                                batterywhereCondition = null;
                            }

                            battery newbattery = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newbattery = CreateNewRecord.NewBattery();
                            }

                            using (Operations<battery> battery_operation = new Operations<battery>(command, batterywhereCondition, newbattery, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (battery_operation.Result != null)
                                {
                                    foreach (var item in battery_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;

                        case "brands":
                            Expression<Func<brands, bool>> brandswhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                brandswhereCondition = x => x.id == id;
                            }
                            else
                            {
                                brandswhereCondition = null;
                            }

                            brands newbrand = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newbrand = CreateNewRecord.NewBrand();
                            }

                            using (Operations<brands> brands_operation = new Operations<brands>(command, brandswhereCondition, newbrand, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (brands_operation.Result != null)
                                {
                                    foreach (var item in brands_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.country.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "builds":
                            Expression<Func<builds, bool>> buildswhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null)
                                || (ParamCollector.Find("build_id") != null && ParamCollector.Find("build_id").Value != null))
                            {
                                buildswhereCondition = x => x.build_id == id;
                            }
                            else
                            {
                                buildswhereCondition = null;
                            }

                            builds newbuild = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newbuild = CreateNewRecord.NewBuild();
                            }

                            using (Operations<builds> builds_operation = new Operations<builds>(command, buildswhereCondition, newbuild, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (builds_operation.Result != null)
                                {
                                    foreach (var item in builds_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.build_id.ToString());
                                        record.Add(item.buildname);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "Cams":
                            Expression<Func<Cams, bool>> camswhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                camswhereCondition = x => x.id == id;
                            }
                            else
                            {
                                camswhereCondition = null;
                            }

                            Cams newcam = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newcam = CreateNewRecord.NewCam();
                            }

                            using (Operations<Cams> cams_operation = new Operations<Cams>(command, camswhereCondition, newcam, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (cams_operation.Result != null)
                                {
                                    foreach (var item in cams_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "ESC":
                            Expression<Func<ESC, bool>> escwhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                escwhereCondition = x => x.id == id;
                            }
                            else
                            {
                                escwhereCondition = null;
                            }

                            ESC newesc = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newesc = CreateNewRecord.NewEsc();
                            }

                            using (Operations<ESC> esc_operation = new Operations<ESC>(command, escwhereCondition, newesc, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (esc_operation.Result != null)
                                {
                                    foreach (var item in esc_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "FC":
                            Expression<Func<FC, bool>> fcwhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                fcwhereCondition = x => x.id == id;
                            }
                            else
                            {
                                fcwhereCondition = null;
                            }

                            FC newfc = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newfc = CreateNewRecord.NewFC();
                            }

                            using (Operations<FC> fc_operation = new Operations<FC>(command, fcwhereCondition, newfc, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (fc_operation.Result != null)
                                {
                                    foreach (var item in fc_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "motors":
                            Expression<Func<motors, bool>> motorwhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                motorwhereCondition = x => x.id == id;
                            }
                            else
                            {
                                motorwhereCondition = null;
                            }

                            motors newmotor = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newmotor = CreateNewRecord.Newmotor();
                            }

                            using (Operations<motors> motor_operation = new Operations<motors>(command, motorwhereCondition, newmotor, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (motor_operation.Result != null)
                                {
                                    foreach (var item in motor_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "PDB":
                            Expression<Func<PDB, bool>> pdbwhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                pdbwhereCondition = x => x.id == id;
                            }
                            else
                            {
                                pdbwhereCondition = null;
                            }

                            PDB newpdb = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newpdb = CreateNewRecord.NewPdb();
                            }

                            using (Operations<PDB> pdb_operation = new Operations<PDB>(command, pdbwhereCondition, newpdb, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (pdb_operation.Result != null)
                                {
                                    foreach (var item in pdb_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "prop":
                            Expression<Func<prop, bool>> propwhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                propwhereCondition = x => x.id == id;
                            }
                            else
                            {
                                propwhereCondition = null;
                            }

                            prop newprop = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newprop = CreateNewRecord.Newprop();
                            }

                            using (Operations<prop> prop_operation = new Operations<prop>(command, propwhereCondition, newprop, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (prop_operation.Result != null)
                                {
                                    foreach (var item in prop_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                        case "VTX":
                            Expression<Func<VTX, bool>> vtxwhereCondition;
                            if ((ParamCollector.Find("ID") != null && ParamCollector.Find("ID").Value != null)
                                || (ParamCollector.Find("id") != null && ParamCollector.Find("id").Value != null))
                            {
                                vtxwhereCondition = x => x.id == id;
                            }
                            else
                            {
                                vtxwhereCondition = null;
                            }

                            VTX newvtx = null;
                            if (command == "Insert" || command == "Update")
                            {
                                newvtx = CreateNewRecord.NewVtx();
                            }

                            using (Operations<VTX> vtx_operation = new Operations<VTX>(command, vtxwhereCondition, newvtx, db))
                            {
                                ParamCollector.QueryResult.Clear();
                                if (vtx_operation.Result != null)
                                {
                                    foreach (var item in vtx_operation.Result)
                                    {
                                        List<string> record = new List<string>();
                                        record.Add(item.id.ToString());
                                        record.Add(item.name);
                                        record.Add(item.brand_id.ToString());
                                        ParamCollector.QueryResult.Add(record);
                                    }
                                }
                            }

                            break;
                    }

                    ParamCollector.Clear();
                    ParamCollector.Add("Table", tablename);
                }
            }

                return true;
        }
    }
}
