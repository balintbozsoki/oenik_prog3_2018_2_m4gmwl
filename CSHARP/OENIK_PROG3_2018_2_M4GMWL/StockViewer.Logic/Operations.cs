﻿// <copyright file="Operations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Data;
    using StockViewer.Logic.Interfaces;
    using StockViewer.Repository;
    using StockViewer.Repository.Interfaces;

    /// <summary>
    /// The operations can be called from here by the console.
    /// </summary>
    /// <typeparam name="T">Type depends on table.</typeparam>
    public class Operations<T> : ILogic<T>, IDisposable
        where T : class
    {
        private readonly DbContext db;
        private readonly SqlRepository<T> sqlRepo;
        private IQueryable<T> result;

        /// <summary>
        /// Initializes a new instance of the <see cref="Operations{T}"/> class.
        /// </summary>
        /// <param name="command">String command coming from the UI which specifies what Repository method to call.</param>
        /// <param name="whereCondition">linq where condition</param>
        /// <param name="record">new record to be inserted</param>
        /// <param name="dBEntities"> dbcontext to run the operation on</param>
        public Operations(string command, Expression<Func<T, bool>> whereCondition, T record, DbContext dBEntities)
        {
            this.result = null;
            this.db = dBEntities;
            this.sqlRepo = new SqlRepository<T>(this.db);

            switch (command)
            {
                case "Find_All":
                    this.result = this.Find(null);
                    break;
                case "Find_By_ID":
                    this.result = this.Find(whereCondition);
                    break;
                case "Insert":
                    this.Insert(record);
                    break;
                case "Update":
                    this.Update(record, whereCondition);
                    break;
                case "Delete":
                    this.Delete(whereCondition);
                    break;
            }
        }

        /// <summary>
        /// Gets or sets the result of the query (IQueryable(T))
        /// </summary>
        public IQueryable<T> Result
        {
            get
            {
                return this.result;
            }

            set
            {
                this.result = value;
            }
        }

        /// <inheritdoc/>
        public void Delete(Expression<Func<T, bool>> whereCondition)
        {
            this.sqlRepo.Delete(whereCondition);
        }

        /// <summary>
        /// implementation of the IDisposable interface
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc/>
        public IQueryable<T> Find(Expression<Func<T, bool>> whereCondition)
        {
            return this.sqlRepo.Find(whereCondition);
        }

        /// <inheritdoc/>
        public void Insert(T entity)
        {
            this.sqlRepo.Insert(entity);
        }

        /// <inheritdoc/>
        public void Update(T entity, Expression<Func<T, bool>> whereCondition)
        {
            this.sqlRepo.Update(entity, whereCondition);
        }

        /// <summary>
        /// implementation of the IDisposable interface
        /// </summary>
        /// <param name="disposing">dispose?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
        }
    }
}
