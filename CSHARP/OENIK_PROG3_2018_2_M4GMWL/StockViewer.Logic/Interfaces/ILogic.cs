﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// interface for the operations
    /// </summary>
    /// <typeparam name="T">Type depends on table.</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// Inserts the record into the table given by the type.
        /// </summary>
        /// <param name="entity">The record to be inserted.</param>
        void Insert(T entity);

        /// <summary>
        /// Update this entity in the DB.
        /// </summary>
        /// <param name="entity">new data to replace the record(s) with specified by the condition</param>
        /// <param name="whereCondition">where to update</param>
        void Update(T entity, Expression<Func<T, bool>> whereCondition);

        /// <summary>
        /// Deletes entry from DB.
        /// </summary>
        /// <param name="whereCondition">deletion condition</param>
        void Delete(Expression<Func<T, bool>> whereCondition);

        /// <summary>
        /// Returns all the rows for type T
        /// </summary>
        /// <param name="whereCondition">condition for which row(s) to return</param>
        /// <returns> Returns all the rows for type T.</returns>
        IQueryable<T> Find(Expression<Func<T, bool>> whereCondition);
    }
}
