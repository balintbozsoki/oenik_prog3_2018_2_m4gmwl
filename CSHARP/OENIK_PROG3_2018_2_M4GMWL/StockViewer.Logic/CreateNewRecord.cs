﻿// <copyright file="CreateNewRecord.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Data;
    using StockViewer.Repository;

    /// <summary>
    /// Creates new table records to be inserted based on the input data stored in the ParamCollector.
    /// </summary>
    public static class CreateNewRecord
    {
        /// <summary>
        /// Creates a new frame instance
        /// </summary>
        /// <returns>returns new frame instance</returns>
        public static frames NewFrame()
        {
            if ((bool)ParamCollector.FindRecord("frames").Value)
            {
                frames frame = new frames
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    size = int.Parse(ParamCollector.Find("size").Value.ToString()),
                    psize = int.Parse(ParamCollector.Find("psize").Value.ToString()),
                    msize = ParamCollector.Find("msize").Value.ToString(),
                    armpiece = ParamCollector.ToBoolean(ParamCollector.Find("armpiece").Value.ToString()),
                    armthickness = int.Parse(ParamCollector.Find("armthickness").Value.ToString()),
                    bplatethickness = int.Parse(ParamCollector.Find("bplatethickness").Value.ToString()),
                    tplatetickness = int.Parse(ParamCollector.Find("tplatetickness").Value.ToString()),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString()),
                    material = ParamCollector.Find("material").Value.ToString()
                };
                return frame;
            }

            return null;
        }

        /// <summary>
        /// Creates a new battery instance
        /// </summary>
        /// <returns>returns new battery instance</returns>
        public static battery NewBattery()
        {
            if ((bool)ParamCollector.FindRecord("battery").Value)
            {
                battery batt = new battery
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    cells = int.Parse(ParamCollector.Find("cells").Value.ToString()),
                    c = int.Parse(ParamCollector.Find("c").Value.ToString()),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return batt;
            }

            return null;
        }

        /// <summary>
        /// Creates a new brands instance
        /// </summary>
        /// <returns>returns new brands instance</returns>
        public static brands NewBrand()
        {
            if ((bool)ParamCollector.FindRecord("brands").Value)
            {
                brands brand = new brands
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    country = ParamCollector.Find("country").Value.ToString()
                };
                return brand;
            }

            return null;
        }

        /// <summary>
        /// Creates a new builds instance
        /// </summary>
        /// <returns>returns new builds instance</returns>
        public static builds NewBuild()
        {
            if ((bool)ParamCollector.FindRecord("builds").Value)
            {
                builds build = new builds
                {
                    build_id = int.Parse(ParamCollector.Find("build_id").Value.ToString()),
                    buildname = ParamCollector.Find("buildname").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    frame_id = int.Parse(ParamCollector.Find("frame_id").Value.ToString()),
                    motor_id = int.Parse(ParamCollector.Find("motor_id").Value.ToString()),
                    esc_id = int.Parse(ParamCollector.Find("esc_id").Value.ToString()),
                    pdb_id = int.Parse(ParamCollector.Find("pdb_id").Value.ToString()),
                    fc_id = int.Parse(ParamCollector.Find("fc_id").Value.ToString()),
                    camera_id = int.Parse(ParamCollector.Find("camera_id").Value.ToString()),
                    vtx_id = int.Parse(ParamCollector.Find("vtx_id").Value.ToString()),
                    prop_id = int.Parse(ParamCollector.Find("prop_id").Value.ToString()),
                    battery_id = int.Parse(ParamCollector.Find("battery_id").Value.ToString())
                };
                return build;
            }

            return null;
        }

        /// <summary>
        /// Creates a new Cams instance
        /// </summary>
        /// <returns>returns new Cams instance</returns>
        public static Cams NewCam()
        {
            if ((bool)ParamCollector.FindRecord("Cams").Value)
            {
                Cams cam = new Cams
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    res = ParamCollector.Find("res").Value.ToString(),
                    pal = ParamCollector.ToBoolean(ParamCollector.Find("pal").Value.ToString()),
                    ntsc = ParamCollector.ToBoolean(ParamCollector.Find("ntsc").Value.ToString()),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return cam;
            }

            return null;
        }

        /// <summary>
        /// Creates a new ESC instance
        /// </summary>
        /// <returns>returns new ESC instance</returns>
        public static ESC NewEsc()
        {
            if ((bool)ParamCollector.FindRecord("ESC").Value)
            {
                ESC esc = new ESC
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    fc_id = int.Parse(ParamCollector.Find("fc_id").Value.ToString()),
                    cells = ParamCollector.Find("cells").Value.ToString(),
                    amp = int.Parse(ParamCollector.Find("amp").Value.ToString()),
                    burst = ParamCollector.Find("burst").Value.ToString(),
                    bamp = int.Parse(ParamCollector.Find("bamp").Value.ToString()),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return esc;
            }

            return null;
        }

        /// <summary>
        /// Creates a new FC instance
        /// </summary>
        /// <returns>returns new FC instance</returns>
        public static FC NewFC()
        {
            if ((bool)ParamCollector.FindRecord("FC").Value)
            {
                FC fc = new FC
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    esc = ParamCollector.ToBoolean(ParamCollector.Find("esc").Value.ToString()),
                    pdb = ParamCollector.ToBoolean(ParamCollector.Find("pdb").Value.ToString()),
                    vtx = ParamCollector.ToBoolean(ParamCollector.Find("vtx").Value.ToString()),
                    osd = ParamCollector.ToBoolean(ParamCollector.Find("osd").Value.ToString()),
                    compass = ParamCollector.ToBoolean(ParamCollector.Find("compass").Value.ToString()),
                    baro = ParamCollector.ToBoolean(ParamCollector.Find("baro").Value.ToString()),
                    bbox = ParamCollector.ToBoolean(ParamCollector.Find("bbox").Value.ToString()),
                    cpu = ParamCollector.Find("cpu").Value.ToString(),
                    os = ParamCollector.Find("os").Value.ToString(),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return fc;
            }

            return null;
        }

        /// <summary>
        /// Creates a new motors instance
        /// </summary>
        /// <returns>returns new motors instance</returns>
        public static motors Newmotor()
        {
            if ((bool)ParamCollector.FindRecord("motors").Value)
            {
                motors motor = new motors
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    size = int.Parse(ParamCollector.Find("size").Value.ToString()),
                    kv = int.Parse(ParamCollector.Find("kv").Value.ToString()),
                    ampdraw = int.Parse(ParamCollector.Find("ampdraw").Value.ToString()),
                    thrust = int.Parse(ParamCollector.Find("thrust").Value.ToString()),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString()),
                    cells = ParamCollector.Find("cells").Value.ToString()
                };
                return motor;
            }

            return null;
        }

        /// <summary>
        /// Creates a new PDB instance
        /// </summary>
        /// <returns>returns new PDB instance</returns>
        public static PDB NewPdb()
        {
            if ((bool)ParamCollector.FindRecord("PDB").Value)
            {
                PDB pdb = new PDB
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    fc_id = int.Parse(ParamCollector.Find("fc_id").Value.ToString()),
                    amp = int.Parse(ParamCollector.Find("amp").Value.ToString()),
                    bec5 = ParamCollector.ToBoolean(ParamCollector.Find("bec5").Value.ToString()),
                    bec12 = ParamCollector.ToBoolean(ParamCollector.Find("bec12").Value.ToString()),
                    motors = int.Parse(ParamCollector.Find("motors").Value.ToString()),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return pdb;
            }

            return null;
        }

        /// <summary>
        /// Creates a new prop instance
        /// </summary>
        /// <returns>returns new prop instance</returns>
        public static prop Newprop()
        {
            if ((bool)ParamCollector.FindRecord("prop").Value)
            {
                prop prp = new prop
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    size = ParamCollector.Find("size").Value.ToString(),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return prp;
            }

            return null;
        }

        /// <summary>
        /// Creates a new VTX instance
        /// </summary>
        /// <returns>returns new VTX instance</returns>
        public static VTX NewVtx()
        {
            if ((bool)ParamCollector.FindRecord("VTX").Value)
            {
                VTX vtx = new VTX
                {
                    id = int.Parse(ParamCollector.Find("id").Value.ToString()),
                    name = ParamCollector.Find("name").Value.ToString(),
                    brand_id = int.Parse(ParamCollector.Find("brand_id").Value.ToString()),
                    fc_id = int.Parse(ParamCollector.Find("fc_id").Value.ToString()),
                    channels = int.Parse(ParamCollector.Find("channels").Value.ToString()),
                    freq = ParamCollector.Find("freq").Value.ToString(),
                    tpower = ParamCollector.Find("tpower").Value.ToString(),
                    mass = int.Parse(ParamCollector.Find("mass").Value.ToString())
                };
                return vtx;
            }

            return null;
        }
    }
}
