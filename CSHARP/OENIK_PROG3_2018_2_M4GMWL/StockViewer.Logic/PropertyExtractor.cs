﻿// <copyright file="PropertyExtractor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Data;
    using StockViewer.Repository;

    /// <summary>
    /// Extracts the properties and their types from the table
    /// </summary>
    public static class PropertyExtractor
    {
        /// <summary>
        /// Extracts the properties and their types from the table type identified by the string argument
        /// </summary>
        /// <param name="table">table type identified by the string argument</param>
        /// <returns>the properties and their types</returns>
        public static List<Param> ExtractProperties(string table)
        {
            List<Param> pl = new List<Param>();
            switch (table)
            {
                case "frames":
                    frames f = new frames();
                    var tf = f.GetType().GetProperties();
                    foreach (var item in tf)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "battery":
                    battery b = new battery();
                    var tb = b.GetType().GetProperties();
                    foreach (var item in tb)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "brands":
                    brands br = new brands();
                    var tbr = br.GetType().GetProperties();
                    for (int i = 0; i < 5; i++)
                    {
                        Param pf = new Param();
                        pf.Name = tbr[i].Name;
                        pf.Value = tbr[i].PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "builds":
                    builds bu = new builds();
                    var tbu = bu.GetType().GetProperties();
                    for (int i = 0; i < 14; i++)
                    {
                        Param pf = new Param();
                        pf.Name = tbu[i].Name;
                        pf.Value = tbu[i].PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "Cams":
                    Cams c = new Cams();
                    var tc = c.GetType().GetProperties();
                    foreach (var item in tc)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "ESC":
                    ESC e = new ESC();
                    var te = e.GetType().GetProperties();
                    foreach (var item in te)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));

                    break;
                case "FC":
                    FC fc = new FC();
                    var tfc = fc.GetType().GetProperties();
                    foreach (var item in tfc)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));
                    pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));
                    pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));

                    break;
                case "motors":
                    motors m = new motors();
                    var tm = m.GetType().GetProperties();
                    foreach (var item in tm)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "PDB":
                    PDB p = new PDB();
                    var tp = p.GetType().GetProperties();
                    foreach (var item in tp)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));
                    break;
                case "prop":
                    prop prp = new prop();
                    var tprp = prp.GetType().GetProperties();
                    foreach (var item in tprp)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    break;
                case "VTX":
                    VTX v = new VTX();
                    var tv = v.GetType().GetProperties();
                    foreach (var item in tv)
                    {
                        Param pf = new Param();
                        pf.Name = item.Name;
                        pf.Value = item.PropertyType.Name;
                        pl.Add(pf);
                    }

                    pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));

                    break;
            }

            pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));
            pl.RemoveAt(pl.IndexOf(pl.LastOrDefault<Param>()));
            return pl;
        }
    }
}
