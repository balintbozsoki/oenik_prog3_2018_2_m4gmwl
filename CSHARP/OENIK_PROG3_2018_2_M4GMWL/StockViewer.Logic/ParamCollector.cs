﻿// <copyright file="ParamCollector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Contains the parameters given by the user.
    /// </summary>
    public static class ParamCollector
    {
        private static List<Param> parameters = new List<Param>();
        private static List<List<string>> queryresult = new List<List<string>>();
        private static List<Param> newrecords = new List<Param>();

        /// <summary>
        /// Gets or sets the list that keeps track of wich tables new records values are stored in the ParamCollector.
        /// </summary>
        public static List<Param> NewRecords
        {
            get
            {
                return newrecords;
            }

            set
            {
                newrecords = value;
            }
        }

        /// <summary>
        /// Gets or sets the string lists of result records.
        /// </summary>
        public static List<List<string>> QueryResult
        {
            get
            {
                return queryresult;
            }

            set
            {
                queryresult = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of parameters given by the user.
        /// </summary>
        public static List<Param> Parameters
        {
            get
            {
                return parameters;
            }

            set
            {
                parameters = value;
            }
        }

        /// <summary>
        /// Add a parameter to the collector.
        /// </summary>
        /// <param name="name">Name of the table.</param>
        /// <param name="value">Value of the parameter.</param>
        /// <returns>Returns true either way....</returns>
        public static bool AddRecordType(string name, object value)
        {
            if (FindRecord(name) != null)
            {
                newrecords.Remove(FindRecord(name));
            }

            Param item = new Param();
            item.Name = name;
            item.Value = value;
            newrecords.Add(item);
            return true;
        }

        /// <summary>
        /// Finds a parameter by name.
        /// </summary>
        /// <param name="name">Name of the parameter.</param>
        /// <returns>Returns null if the parameter cannot be found.</returns>
        public static Param FindRecord(string name)
        {
            Param param = null;
            param = newrecords.Find(x => x.Name == name);
            return param;
        }

        /// <summary>
        /// Add a parameter to the collector.
        /// </summary>
        /// <param name="name">Name of the parameter.</param>
        /// <param name="value">Value of the parameter.</param>
        /// <returns>Returns true either way....</returns>
        public static bool Add(string name, object value)
        {
            if (Find(name) != null)
            {
                parameters.Remove(Find(name));
            }

            Param item = new Param();
            item.Name = name;
            item.Value = value;
            parameters.Add(item);
            return true;
        }

        /// <summary>
        /// Clears the collectors list;
        /// </summary>
        public static void Clear()
        {
            parameters.Clear();
        }

        /// <summary>
        /// Finds a parameter by name.
        /// </summary>
        /// <param name="name">Name of the parameter.</param>
        /// <returns>Returns null if the parameter cannot be found.</returns>
        public static Param Find(string name)
        {
            Param param = null;
            param = parameters.Find(x => x.Name == name);
            return param;
        }

        /// <summary>
        /// Decides if a string is bool and if it is than its value
        /// </summary>
        /// <param name="value">input string</param>
        /// <returns>returns the converted value or false</returns>
        public static bool ToBoolean(string value)
        {
            switch (value.ToLower())
            {
                case "true":
                    return true;
                case "t":
                    return true;
                case "1":
                    return true;
                case "0":
                    return false;
                case "false":
                    return false;
                case "f":
                    return false;
            }

            return false;
        }
    }
}
