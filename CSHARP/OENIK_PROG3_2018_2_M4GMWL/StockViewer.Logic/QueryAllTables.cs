﻿// <copyright file="QueryAllTables.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Data;
    using StockViewer.Repository;

    /// <summary>
    /// queries all tables
    /// </summary>
    public class QueryAllTables : IDisposable
    {
        private Operations<battery> battery;
        private Operations<brands> brands;
        private Operations<builds> builds;
        private Operations<Cams> cams;
        private Operations<ESC> esc;
        private Operations<FC> fc;
        private Operations<frames> frames;
        private Operations<motors> motors;
        private Operations<PDB> pdb;
        private Operations<prop> prop;
        private Operations<VTX> vtx;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryAllTables"/> class.
        /// Queries all tables
        /// </summary>
        /// <param name="db">dbcontext to query</param>
        // <param name="testMode">Decides if the operations should run on the testing database.</param>
        public QueryAllTables(DbContext db)
        {
            this.battery = new Operations<battery>("Find_All", null, null, db);
            this.brands = new Operations<brands>("Find_All", null, null, db);
            this.builds = new Operations<builds>("Find_All", null, null, db);
            this.cams = new Operations<Cams>("Find_All", null, null, db);
            this.esc = new Operations<ESC>("Find_All", null, null, db);
            this.fc = new Operations<FC>("Find_All", null, null, db);
            this.frames = new Operations<frames>("Find_All", null, null, db);
            this.motors = new Operations<motors>("Find_All", null, null, db);
            this.pdb = new Operations<PDB>("Find_All", null, null, db);
            this.prop = new Operations<prop>("Find_All", null, null, db);
            this.vtx = new Operations<VTX>("Find_All", null, null, db);
        }

        /// <summary>
        /// Gets an Operations(VTX) instance.
        /// </summary>
        public Operations<VTX> Vtx
        {
            get
            {
                return this.vtx;
            }

            private set
            {
                this.vtx = value;
            }
        }

        /// <summary>
        /// Gets an Operations(prop) instance.
        /// </summary>
        public Operations<prop> Prop
        {
            get
            {
                return this.prop;
            }

            private set
            {
                this.prop = value;
            }
        }

        /// <summary>
        /// Gets an Operations(PDB) instance.
        /// </summary>
        public Operations<PDB> Pdb
        {
            get
            {
                return this.pdb;
            }

            private set
            {
                this.pdb = value;
            }
        }

        /// <summary>
        /// Gets an Operations(motors) instance.
        /// </summary>
        public Operations<motors> Motors
        {
            get
            {
                return this.motors;
            }

            private set
            {
                this.motors = value;
            }
        }

        /// <summary>
        /// Gets an Operations(frames) instance.
        /// </summary>
        public Operations<frames> Frames
        {
            get
            {
                return this.frames;
            }

            private set
            {
                this.frames = value;
            }
        }

        /// <summary>
        /// Gets an Operations(FC) instance.
        /// </summary>
        public Operations<FC> FC
        {
            get
            {
                return this.fc;
            }

            private set
            {
                this.fc = value;
            }
        }

        /// <summary>
        /// Gets an Operations(ESC) instance.
        /// </summary>
        public Operations<ESC> Esc
        {
            get
            {
                return this.esc;
            }

            private set
            {
                this.esc = value;
            }
        }

        /// <summary>
        /// Gets an Operations(cams) instance.
        /// </summary>
        public Operations<Cams> Cam
        {
            get
            {
                return this.cams;
            }

            private set
            {
                this.cams = value;
            }
        }

        /// <summary>
        /// Gets an Operations(builds) instance.
        /// </summary>
        public Operations<builds> Builds
        {
            get
            {
                return this.builds;
            }

            private set
            {
                this.builds = value;
            }
        }

        /// <summary>
        /// Gets an Operations(brands) instance.
        /// </summary>
        public Operations<brands> Brands
        {
            get
            {
                return this.brands;
            }

            private set
            {
                this.brands = value;
            }
        }

        /// <summary>
        /// Gets an Operations(battery) instance.
        /// </summary>
        public Operations<battery> Battery
        {
            get
            {
                return this.battery;
            }

            private set
            {
                this.battery = value;
            }
        }

        /// <summary>
        /// implementation of the IDisposable interface
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// implementation of the IDisposable interface
        /// </summary>
        /// <param name="disposing">dispose?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.battery.Dispose();
                this.builds.Dispose();
                this.cams.Dispose();
                this.esc.Dispose();
                this.fc.Dispose();
                this.frames.Dispose();
                this.motors.Dispose();
                this.pdb.Dispose();
                this.prop.Dispose();
                this.vtx.Dispose();
                this.brands.Dispose();
            }
        }
    }
}
