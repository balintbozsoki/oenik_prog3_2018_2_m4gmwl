﻿// <copyright file="ComplexOperations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Data;
    using StockViewer.Repository;

    /// <summary>
    /// Contains the methods for non-CRUD operations
    /// </summary>
    public class ComplexOperations
    {
        /// <summary>
        /// Saves all the build records with their mass into the ParamCollectors QueryResult.
        /// </summary>
        /// <param name="builds">Builds to list.</param>
        public void Get_Build_Mass(IQueryable<builds> builds)
        {
            var bmass = from bld in builds
                        select new
                        {
                            Id = bld.build_id,
                            Name = bld.buildname,
                            Mass = bld.battery.mass + bld.frames.mass + ((bld.motors.mass + (bld.ESC.mass ?? 0) + bld.prop.mass) * 4) +
                            (bld.PDB.mass ?? 0) + bld.FC.mass + bld.Cams.mass + (bld.VTX.mass ?? 0)
                        };
            ParamCollector.QueryResult.Clear();
            ParamCollector.Clear();
            foreach (var item in bmass)
            {
                List<string> record = new List<string>();
                record.Add(item.Id.ToString());
                record.Add(item.Name);
                record.Add(item.Mass.ToString());
                ParamCollector.QueryResult.Add(record);
            }
        }

        /// <summary>
        /// Saves all the build records with their thrust to weight ratio into the ParamCollectors QueryResult.
        /// </summary>
        /// <param name="builds">Builds to list.</param>
        public void List_Build_Twr(IQueryable<builds> builds)
        {
            var btwr = from bld in builds
                       select new
                       {
                           Id = bld.build_id,
                           Name = bld.buildname,
                           Mass = bld.battery.mass + bld.frames.mass + ((bld.motors.mass + (bld.ESC.mass ?? 0) + bld.prop.mass) * 4) +
                           (bld.PDB.mass ?? 0) + bld.FC.mass + bld.Cams.mass + (bld.VTX.mass ?? 0),
                           Thrust = bld.motors.thrust * 4
                       };
            ParamCollector.QueryResult.Clear();
            foreach (var item in btwr)
            {
                List<string> record = new List<string>();
                record.Add(item.Id.ToString());
                record.Add(item.Name);
                double twr = (double)item.Thrust / (double)item.Mass;
                record.Add(twr.ToString());
                ParamCollector.QueryResult.Add(record);
            }
        }

        /// <summary>
        /// Saves all the builds records with the number of products for each brand to the ParamCollectors QueryResult.
        /// </summary>
        /// <param name="brand">Brands to list.</param>
        public void List_Brand_Products(IQueryable<brands> brand)
        {
            var products = from brands in brand
                           select new
                           {
                               Id = brands.id,
                               Name = brands.name,
                               Count = brands.battery.Count + brands.builds.Count + brands.Cams.Count +
                               brands.ESC.Count + brands.FC.Count + brands.frames.Count + brands.motors.Count +
                               brands.PDB.Count + brands.prop.Count + brands.VTX.Count
                           };
            ParamCollector.QueryResult.Clear();
            foreach (var item in products)
            {
                List<string> record = new List<string>();
                record.Add(item.Id.ToString());
                record.Add(item.Name);
                record.Add(item.Count.ToString());
                ParamCollector.QueryResult.Add(record);
            }
        }
    }
}
