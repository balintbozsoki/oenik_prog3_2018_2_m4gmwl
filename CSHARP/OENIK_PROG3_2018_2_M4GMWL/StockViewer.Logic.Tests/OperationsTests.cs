﻿// <copyright file="OperationsTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using StockViewer.Data;
    using StockViewer.Logic;
    using StockViewer.Logic.Interfaces;
    using StockViewer.Logic.Tests;
    using StockViewer.Repository;

    /// <summary>
    /// Test class for the Operations class.
    /// </summary>
    [TestFixture]
    public class OperationsTests : IDisposable
    {
        private TestMultirotorDBEntities testdb;

        private QueryAllTables queryall;
        private Mock<ILogic<frames>> mockframeoperation;
        private Mock<ILogic<builds>> mockbuildoperation;
        private Mock<ILogic<battery>> mockbatteryoperation;
        private Mock<ILogic<Cams>> mockcamoperation;
        private Mock<ILogic<ESC>> mockescoperation;
        private Mock<ILogic<FC>> mockfcoperation;
        private Mock<ILogic<motors>> mockmotorsoperation;
        private Mock<ILogic<PDB>> mockpdboperation;
        private Mock<ILogic<prop>> mockpropoperation;
        private Mock<ILogic<VTX>> mockvtxoperation;

        /// <summary>
        /// setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.testdb = new TestMultirotorDBEntities();

            List<builds> mybuilds = new List<builds>()
            {
                new builds()
                {
                    build_id = 1,
                    buildname = "100$ build",
                    brand_id = null,
                    frame_id = 1,
                    motor_id = 1,
                    esc_id = 2,
                    pdb_id = 3,
                    fc_id = 1,
                    camera_id = 1,
                    vtx_id = 2,
                    prop_id = 1,
                    battery_id = 1
                },
                new builds()
                {
                    build_id = 2,
                    buildname = "racer",
                    brand_id = null,
                    frame_id = 2,
                    motor_id = 3,
                    esc_id = 5,
                    pdb_id = 5,
                    fc_id = 4,
                    camera_id = 3,
                    vtx_id = 1,
                    prop_id = 3,
                    battery_id = 1
                },
                new builds()
                {
                    build_id = 3,
                    buildname = "Wizard X220S",
                    brand_id = 1,
                    frame_id = 4,
                    motor_id = 2,
                    esc_id = 6,
                    pdb_id = 2,
                    fc_id = 5,
                    camera_id = 1,
                    vtx_id = 2,
                    prop_id = 2,
                    battery_id = 2
                },
                new builds()
                {
                    build_id = 4,
                    buildname = "Crusader GT2",
                    brand_id = 4,
                    frame_id = 3,
                    motor_id = 3,
                    esc_id = 2,
                    pdb_id = 2,
                    fc_id = 1,
                    camera_id = 2,
                    vtx_id = 5,
                    prop_id = 5,
                    battery_id = 3
                }
            };
            List<frames> myframes = new List<frames>()
            {
                new frames()
                {
                    id = 1,
                    name = "Lisam LS-210",
                    brand_id = 15,
                    size = 210,
                    psize = 5,
                    msize = "1806,2204",
                    armpiece = false,
                    armthickness = 3,
                    bplatethickness = 3,
                    tplatetickness = 2,
                    mass = 100,
                    material = "3k carbon"
                },
                new frames()
                {
                    id = 2,
                    name = "Real1",
                    brand_id = 9,
                    size = 220,
                    psize = 5,
                    msize = "2205,2306",
                    armpiece = true,
                    armthickness = 4,
                    bplatethickness = 2,
                    tplatetickness = 2,
                    mass = 104,
                    material = "carbon,aluminum"
                },
                new frames()
                {
                    id = 3,
                    name = "Crusader GT2 200",
                    brand_id = 4,
                    size = 200,
                    psize = 5,
                    msize = "2205",
                    armpiece = false,
                    armthickness = 5,
                    bplatethickness = 5,
                    tplatetickness = null,
                    mass = 95,
                    material = "3k carbon"
                },
                new frames()
                {
                    id = 4,
                    name = "Wizard x220S",
                    brand_id = 1,
                    size = 220,
                    psize = 5,
                    msize = "2205",
                    armpiece = true,
                    armthickness = 2,
                    bplatethickness = 2,
                    tplatetickness = 2,
                    mass = 130,
                    material = "carbon,glass fiber"
                },
                new frames()
                {
                    id = 5,
                    name = "LMartian II",
                    brand_id = 15,
                    size = 220,
                    psize = 5,
                    msize = "1806,2204,2205,2206",
                    armpiece = false,
                    armthickness = 4,
                    bplatethickness = 2,
                    tplatetickness = 2,
                    mass = 130,
                    material = "3k carbon"
                },
                new frames()
                {
                    id = 6,
                    name = "testframe",
                    brand_id = 15,
                    size = 210,
                    psize = 5,
                    msize = "1806,2204",
                    armpiece = false,
                    armthickness = 3,
                    bplatethickness = 3,
                    tplatetickness = 2,
                    mass = 100,
                    material = "3k carbon"
                }
            };
            List<battery> mybatteries = new List<battery>()
            {
                new battery()
                {
                    id = 1,
                    name = "Giant Power Mega Graphene 2.0 14.8V 1550mAh",
                    brand_id = 3,
                    cells = 4,
                    c = 75,
                    mass = 160
                },
                new battery()
                {
                    id = 2,
                    name = "Wizard X220S FPV Racer Spare Part 4S 14.8V 1500mAh",
                    brand_id = 1,
                    cells = 4,
                    c = 75,
                    mass = 177
                },
                new battery()
                {
                    id = 3,
                    name = "Giant Power 1500mAh 14.8V",
                    brand_id = 3,
                    cells = 4,
                    c = 65,
                    mass = 160
                },
                new battery()
                {
                    id = 4,
                    name = "11.1V 1500mAh",
                    brand_id = 12,
                    cells = 3,
                    c = 40,
                    mass = 113
                },
                new battery()
                {
                    id = 5,
                    name = "Giant Power Mega Graphene 18.5V 1350mAh",
                    brand_id = 3,
                    cells = 5,
                    c = 75,
                    mass = 190
                }
            };
            List<Cams> mycams = new List<Cams>()
            {
                new Cams()
                {
                    id = 1,
                    name = "CCD 110 Degree 2.8mm Lens Mini FPV camera",
                    brand_id = 1,
                    res = "1000TVL",
                    pal = true,
                    ntsc = true,
                    mass = 10
                },
                new Cams()
                {
                    id = 2,
                    name = "Swift 2",
                    brand_id = 7,
                    res = "600TVL",
                    pal = true,
                    ntsc = false,
                    mass = 14
                },
                new Cams()
                {
                    id = 3,
                    name = "Monster",
                    brand_id = 13,
                    res = "1200TVL",
                    pal = true,
                    ntsc = true,
                    mass = 15
                },
                new Cams()
                {
                    id = 4,
                    name = "Split 2",
                    brand_id = 7,
                    res = "HD",
                    pal = true,
                    ntsc = true,
                    mass = 21
                },
                new Cams()
                {
                    id = 5,
                    name = "Split",
                    brand_id = 7,
                    res = "HD",
                    pal = true,
                    ntsc = true,
                    mass = 21
                }
            };
            List<ESC> myescs = new List<ESC>()
            {
                new ESC()
                {
                    id = 1,
                    name = "RS20A",
                    brand_id = 2,
                    fc_id = null,
                    cells = "2-4S",
                    amp = 20,
                    burst = "10s/m",
                    bamp = 25,
                    mass = 8
                },
                new ESC()
                {
                    id = 2,
                    name = "RS30A V2",
                    brand_id = 2,
                    fc_id = null,
                    cells = "2-4S",
                    amp = 30,
                    burst = "10s/m",
                    bamp = 35,
                    mass = 6
                },
                new ESC()
                {
                    id = 3,
                    name = "Simonk Series",
                    brand_id = 14,
                    fc_id = null,
                    cells = "2-6S",
                    amp = 40,
                    burst = "10s/m",
                    bamp = 50,
                    mass = 41
                },
                new ESC()
                {
                    id = 4,
                    name = "TattooF4S",
                    brand_id = 2,
                    fc_id = 3,
                    cells = "2-4S",
                    amp = 30,
                    burst = "10s/m",
                    bamp = 35,
                    mass = null
                },
                new ESC()
                {
                    id = 5,
                    name = "Magnum",
                    brand_id = 14,
                    fc_id = 4,
                    cells = "2-4S",
                    amp = 30,
                    burst = "10s/m",
                    bamp = 35,
                    mass = null
                },
                new ESC()
                {
                    id = 6,
                    name = "Eachine BLHELI 4 in 1 ESC",
                    brand_id = 1,
                    fc_id = null,
                    cells = "2-4S",
                    amp = 30,
                    burst = "10s/m",
                    bamp = 35,
                    mass = 28
                }
            };
            List<FC> myfcs = new List<FC>()
            {
                new FC()
                {
                id = 1,
                name = "F3 Deluxe",
                brand_id = 8,
                esc = false,
                pdb = false,
                vtx = false,
                osd = false,
                compass = true,
                baro = true,
                bbox = true,
                cpu = "F3",
                os = "Cleanflight,Betaflight",
                mass = 6
                },
                new FC()
                {
                id = 2,
                name = "F405 AIO",
                brand_id = 6,
                esc = false,
                pdb = true,
                vtx = false,
                osd = true,
                compass = true,
                baro = true,
                bbox = true,
                cpu = "F405",
                os = "Cleanflight,Betaflight",
                mass = 12
                },
                new FC()
                {
                id = 3,
                name = "TattooF4S",
                brand_id = 2,
                esc = true,
                pdb = true,
                vtx = false,
                osd = true,
                compass = false,
                baro = false,
                bbox = false,
                cpu = "F4",
                os = "Cleanflight,Betaflight",
                mass = 22
                },
                new FC()
                {
                id = 4,
                name = "Magnum",
                brand_id = 14,
                esc = true,
                pdb = false,
                vtx = true,
                osd = true,
                compass = true,
                baro = true,
                bbox = true,
                cpu = "F4",
                os = "Cleanflight,Betaflight",
                mass = 22
                },
                new FC()
                {
                id = 5,
                name = "Omnibus F4",
                brand_id = 1,
                esc = false,
                pdb = false,
                vtx = false,
                osd = false,
                compass = false,
                baro = false,
                bbox = false,
                cpu = "F4",
                os = "Betaflight,dRonin",
                mass = 6
                }
            };
            List<motors> mymotors = new List<motors>()
            {
                new motors()
                {
                    id = 1,
                    name = "BR2205",
                    brand_id = 2,
                    size = 2205,
                    kv = 2300,
                    ampdraw = 27,
                    thrust = 950,
                    mass = 28,
                    cells = "2-4S"
                },
                new motors()
                {
                    id = 2,
                    name = "MN 2206",
                    brand_id = 1,
                    size = 2206,
                    kv = 2300,
                    ampdraw = 28,
                    thrust = 1100,
                    mass = 28,
                    cells = "3-5S"
                },
                new motors()
                {
                    id = 3,
                    name = "EMAX 2205S",
                    brand_id = 14,
                    size = 2205,
                    kv = 2300,
                    ampdraw = 28,
                    thrust = 1180,
                    mass = 29,
                    cells = "3-4S"
                },
                new motors()
                {
                    id = 4,
                    name = "X2814",
                    brand_id = 10,
                    size = 2814,
                    kv = 900,
                    ampdraw = 60,
                    thrust = 2360,
                    mass = 108,
                    cells = "3-4S"
                },
                new motors()
                {
                    id = 5,
                    name = "BR2406S",
                    brand_id = 2,
                    size = 2406,
                    kv = 2600,
                    ampdraw = 42,
                    thrust = 1480,
                    mass = 35,
                    cells = "2-4S"
                }
            };
            List<PDB> mypdbs = new List<PDB>()
            {
                new PDB()
                {
                    id = 1,
                    name = "F405 AIO",
                    brand_id = 6,
                    fc_id = 2,
                    amp = 120,
                    bec5 = true,
                    bec12 = true,
                    motors = 4,
                    mass = null
                },
                new PDB()
                {
                    id = 2,
                    name = "PDB-XPW",
                    brand_id = 9,
                    fc_id = null,
                    amp = 140,
                    bec5 = true,
                    bec12 = true,
                    motors = 4,
                    mass = 7
                },
                new PDB()
                {
                    id = 3,
                    name = "PDB-XT60",
                    brand_id = 6,
                    fc_id = null,
                    amp = 100,
                    bec5 = true,
                    bec12 = true,
                    motors = 6,
                    mass = 8
                },
                new PDB()
                {
                    id = 4,
                    name = "Diatone V8.3 LC Filter Power PDB Board Low Ripple Current",
                    brand_id = 4,
                    fc_id = null,
                    amp = 80,
                    bec5 = true,
                    bec12 = true,
                    motors = 4,
                    mass = 6
                },
                new PDB()
                {
                    id = 5,
                    name = "HUBOSD ECO",
                    brand_id = 9,
                    fc_id = null,
                    amp = 140,
                    bec5 = true,
                    bec12 = true,
                    motors = 4,
                    mass = 9
                }
            };
            List<prop> myprops = new List<prop>()
            {
                new prop()
                {
                    id = 1,
                    name = "Kingkong 5045",
                    brand_id = 11,
                    size = "5x4.5",
                    mass = 4
                },
                new prop()
                {
                    id = 2,
                    name = "X220S 5051 3 Blade Propeller",
                    brand_id = 1,
                    size = "5",
                    mass = 5
                },
                new prop()
                {
                    id = 3,
                    name = "Cyclone T5046C",
                    brand_id = 5,
                    size = "5",
                    mass = 5
                },
                new prop()
                {
                    id = 4,
                    name = "Kingkong 6040",
                    brand_id = 11,
                    size = "6",
                    mass = 6
                },
                new prop()
                {
                    id = 5,
                    name = "5040 Super Durable",
                    brand_id = 4,
                    size = "5",
                    mass = 4
                }
            };
            List<VTX> myvtx = new List<VTX>()
            {
                new VTX()
                {
                    id = 1,
                    name = "Magnum",
                    fc_id = 4,
                    brand_id = 14,
                    channels = 48,
                    freq = "5.8ghz",
                    tpower = "200mw",
                    mass = null
                },
                new VTX()
                {
                    id = 2,
                    name = "TX801",
                    fc_id = null,
                    brand_id = 1,
                    channels = 72,
                    freq = "5.8ghz",
                    tpower = "600mw",
                    mass = 7
                },
                new VTX()
                {
                    id = 3,
                    name = "VTX01 Super Mini",
                    fc_id = null,
                    brand_id = 1,
                    channels = 40,
                    freq = "5.8ghz",
                    tpower = "25mw",
                    mass = 3
                },
                new VTX()
                {
                    id = 4,
                    name = "GX210",
                    fc_id = null,
                    brand_id = 9,
                    channels = 40,
                    freq = "5.8ghz",
                    tpower = "200mw",
                    mass = 6
                },
                new VTX()
                {
                    id = 5,
                    name = "TM200",
                    fc_id = null,
                    brand_id = 13,
                    channels = 40,
                    freq = "5.8ghz",
                    tpower = "200mw",
                    mass = 10
                }
            };

            this.mockvtxoperation = new Mock<ILogic<VTX>>();
            this.mockvtxoperation.Setup(x => x.Find(null)).Returns(myvtx.AsQueryable());
            this.mockpropoperation = new Mock<ILogic<prop>>();
            this.mockpropoperation.Setup(x => x.Find(null)).Returns(myprops.AsQueryable());
            this.mockpdboperation = new Mock<ILogic<PDB>>();
            this.mockpdboperation.Setup(x => x.Find(null)).Returns(mypdbs.AsQueryable());
            this.mockmotorsoperation = new Mock<ILogic<motors>>();
            this.mockmotorsoperation.Setup(x => x.Find(null)).Returns(mymotors.AsQueryable());
            this.mockfcoperation = new Mock<ILogic<FC>>();
            this.mockfcoperation.Setup(x => x.Find(null)).Returns(myfcs.AsQueryable());
            this.mockescoperation = new Mock<ILogic<ESC>>();
            this.mockescoperation.Setup(x => x.Find(null)).Returns(myescs.AsQueryable());
            this.mockcamoperation = new Mock<ILogic<Cams>>();
            this.mockcamoperation.Setup(x => x.Find(null)).Returns(mycams.AsQueryable());
            this.mockbatteryoperation = new Mock<ILogic<battery>>();
            this.mockbatteryoperation.Setup(x => x.Find(null)).Returns(mybatteries.AsQueryable());
            this.mockbuildoperation = new Mock<ILogic<builds>>();
            this.mockbuildoperation.Setup(x => x.Find(null)).Returns(mybuilds.AsQueryable());
            this.mockframeoperation = new Mock<ILogic<frames>>();
            this.mockframeoperation.Setup(x => x.Find(null)).Returns(myframes.AsQueryable());

            this.queryall = new QueryAllTables(this.testdb);
        }

        /// <summary>
        /// tests if all VTX records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllVtx()
        {
            this.mockvtxoperation.Object.Find(null);
            this.mockvtxoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a VTX record by id
        /// </summary>
        [Test]
        public void GetVtxById()
        {
            this.mockvtxoperation.Object.Find(x => x.id == 2);
            this.mockvtxoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new VTX record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertVtx()
        {
            var newvtx = new VTX()
            {
                id = 9,
                name = "test vtx",
                fc_id = 4,
                brand_id = 14,
                channels = 48,
                freq = "5.8ghz",
                tpower = "200mw",
                mass = null
            };

            this.mockvtxoperation.Object.Insert(newvtx);
            this.mockvtxoperation.Verify(x => x.Insert(newvtx), Times.Once);
        }

        /// <summary>
        /// tests if a VTX record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteVtx()
        {
            this.mockvtxoperation.Object.Delete(x => x.id == 2);
            this.mockvtxoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a VTX record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateVtx()
        {
            VTX vtx1 = new VTX()
            {
                id = 1,
                name = "Magnum",
                fc_id = 4,
                brand_id = 14,
                channels = 40,
                freq = "5.8ghz",
                tpower = "200mw",
                mass = null
            };
            this.mockvtxoperation.Object.Update(vtx1, x => x.id == vtx1.id);
            this.mockvtxoperation.Verify(x => x.Update(vtx1, y => y.id == vtx1.id));
        }

        /// <summary>
        /// tests if all prop records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllprop()
        {
            this.mockpropoperation.Object.Find(null);
            this.mockpropoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a prop record by id
        /// </summary>
        [Test]
        public void GetpropById()
        {
            this.mockpropoperation.Object.Find(x => x.id == 2);
            this.mockpropoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new prop record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void Insertprop()
        {
            var newprop = new prop()
            {
                id = 9,
                name = "test prop",
                brand_id = 11,
                size = "5x4.5",
                mass = 4
            };

            this.mockpropoperation.Object.Insert(newprop);
            this.mockpropoperation.Verify(x => x.Insert(newprop), Times.Once);
        }

        /// <summary>
        /// tests if a prop record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void Deleteprop()
        {
            this.mockpropoperation.Object.Delete(x => x.id == 2);
            this.mockpropoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a prop record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void Updateprop()
        {
            prop prop1 = new prop()
            {
                id = 1,
                name = "Kingkong 5045",
                brand_id = 11,
                size = "5x4.5",
                mass = 3
            };
            this.mockpropoperation.Object.Update(prop1, x => x.id == prop1.id);
            this.mockpropoperation.Verify(x => x.Update(prop1, y => y.id == prop1.id));
        }

        /// <summary>
        /// tests if all PDB records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllPdb()
        {
            this.mockpdboperation.Object.Find(null);
            this.mockpdboperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a PDB record by id
        /// </summary>
        [Test]
        public void GetPdbyId()
        {
            this.mockpdboperation.Object.Find(x => x.id == 2);
            this.mockpdboperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new PDB record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertPdb()
        {
            var newPDB = new PDB()
            {
                id = 9,
                name = "test pdb",
                brand_id = 6,
                fc_id = 2,
                amp = 120,
                bec5 = true,
                bec12 = true,
                motors = 4,
                mass = null
            };

            this.mockpdboperation.Object.Insert(newPDB);
            this.mockpdboperation.Verify(x => x.Insert(newPDB), Times.Once);
        }

        /// <summary>
        /// tests if a PDB record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeletePdb()
        {
            this.mockpdboperation.Object.Delete(x => x.id == 2);
            this.mockpdboperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a PDB record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdatePdb()
        {
            PDB pDB1 = new PDB()
            {
                id = 1,
                name = "F405 AIO",
                brand_id = 6,
                fc_id = 2,
                amp = 130,
                bec5 = true,
                bec12 = true,
                motors = 4,
                mass = null
            };
            this.mockpdboperation.Object.Update(pDB1, x => x.id == pDB1.id);
            this.mockpdboperation.Verify(x => x.Update(pDB1, y => y.id == pDB1.id));
        }

        /// <summary>
        /// tests if all motors records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllmotors()
        {
            this.mockmotorsoperation.Object.Find(null);
            this.mockmotorsoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a motors record by id
        /// </summary>
        [Test]
        public void GetmotoryId()
        {
            this.mockmotorsoperation.Object.Find(x => x.id == 2);
            this.mockmotorsoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new motors record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void Insertmotor()
        {
            var newmotor = new motors()
            {
                id = 9,
                name = "test motor",
                brand_id = 2,
                size = 2205,
                kv = 2300,
                ampdraw = 27,
                thrust = 950,
                mass = 28,
                cells = "2-4S"
            };

            this.mockmotorsoperation.Object.Insert(newmotor);
            this.mockmotorsoperation.Verify(x => x.Insert(newmotor), Times.Once);
        }

        /// <summary>
        /// tests if a motors record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void Deletemotor()
        {
            this.mockmotorsoperation.Object.Delete(x => x.id == 2);
            this.mockmotorsoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a motors record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void Updatemotor()
        {
            motors fc1 = new motors()
            {
                id = 1,
                name = "BR2205",
                brand_id = 2,
                size = 2205,
                kv = 2300,
                ampdraw = 28,
                thrust = 1050,
                mass = 30,
                cells = "2-4S"
            };
            this.mockmotorsoperation.Object.Update(fc1, x => x.id == fc1.id);
            this.mockmotorsoperation.Verify(x => x.Update(fc1, y => y.id == fc1.id));
        }

        /// <summary>
        /// tests if all FC records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllFCs()
        {
            this.mockfcoperation.Object.Find(null);
            this.mockfcoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a FC record by id
        /// </summary>
        [Test]
        public void GetFCById()
        {
            this.mockfcoperation.Object.Find(x => x.id == 2);
            this.mockfcoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new FC record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertFC()
        {
            var newFC = new FC()
            {
                id = 9,
                name = "test FC",
                brand_id = 8,
                esc = false,
                pdb = false,
                vtx = false,
                osd = false,
                compass = true,
                baro = true,
                bbox = true,
                cpu = "F3",
                os = "Cleanflight,Betaflight",
                mass = 6
            };

            this.mockfcoperation.Object.Insert(newFC);
            this.mockfcoperation.Verify(x => x.Insert(newFC), Times.Once);
        }

        /// <summary>
        /// tests if a FC record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteFC()
        {
            this.mockfcoperation.Object.Delete(x => x.id == 2);
            this.mockfcoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a FC record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateFC()
        {
            FC fc1 = new FC()
            {
                id = 1,
                name = "F3 Deluxe",
                brand_id = 8,
                esc = false,
                pdb = false,
                vtx = false,
                osd = false,
                compass = true,
                baro = true,
                bbox = true,
                cpu = "F3",
                os = "Cleanflight,Betaflight",
                mass = 16
            };
            this.mockfcoperation.Object.Update(fc1, x => x.id == fc1.id);
            this.mockfcoperation.Verify(x => x.Update(fc1, y => y.id == fc1.id));
        }

        /// <summary>
        /// tests if all ESC records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllEscs()
        {
            this.mockescoperation.Object.Find(null);
            this.mockescoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a ESC record by id
        /// </summary>
        [Test]
        public void GetEscById()
        {
            this.mockescoperation.Object.Find(x => x.id == 2);
            this.mockescoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new ESC record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertEsc()
        {
            var newESC = new ESC()
            {
                id = 9,
                name = "testesc",
                brand_id = 2,
                fc_id = null,
                cells = "2-4S",
                amp = 20,
                burst = "10s/m",
                bamp = 25,
                mass = 8
            };

            this.mockescoperation.Object.Insert(newESC);
            this.mockescoperation.Verify(x => x.Insert(newESC), Times.Once);
        }

        /// <summary>
        /// tests if a ESC record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteEsc()
        {
            this.mockescoperation.Object.Delete(x => x.id == 2);
            this.mockescoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a ESC record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateEsc()
        {
            ESC esc1 = new ESC()
            {
                id = 1,
                name = "RS20A",
                brand_id = 2,
                fc_id = null,
                cells = "2-4S",
                amp = 20,
                burst = "10s/m",
                bamp = 25,
                mass = 8
            };
            this.mockescoperation.Object.Update(esc1, x => x.id == esc1.id);
            this.mockescoperation.Verify(x => x.Update(esc1, y => y.id == esc1.id));
        }

        /// <summary>
        /// tests if all Cams records are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllCams()
        {
            this.mockcamoperation.Object.Find(null);
            this.mockcamoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a Cams record by id
        /// </summary>
        [Test]
        public void GetCamById()
        {
            this.mockcamoperation.Object.Find(x => x.id == 2);
            this.mockcamoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new Cams record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertCam()
        {
            var newCam = new Cams()
            {
                id = 9,
                name = "testcam",
                brand_id = 1,
                res = "1000TVL",
                pal = true,
                ntsc = true,
                mass = 10
            };

            this.mockcamoperation.Object.Insert(newCam);
            this.mockcamoperation.Verify(x => x.Insert(newCam), Times.Once);
        }

        /// <summary>
        /// tests if a Cams record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteCam()
        {
            this.mockcamoperation.Object.Delete(x => x.id == 2);
            this.mockcamoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a Cams record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateCam()
        {
            Cams cam1 = new Cams()
            {
                id = 1,
                name = "CCD 110 Degree 2.8mm Lens Mini FPV camera",
                brand_id = 1,
                res = "hd",
                pal = true,
                ntsc = true,
                mass = 15
            };
            this.mockcamoperation.Object.Update(cam1, x => x.id == cam1.id);
            this.mockcamoperation.Verify(x => x.Update(cam1, y => y.id == cam1.id));
        }

        /// <summary>
        /// tests if all battery recurds are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllBatteries()
        {
            this.mockbatteryoperation.Object.Find(null);
            this.mockbatteryoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a battery record by id
        /// </summary>
        [Test]
        public void GetBatteryById()
        {
            this.mockbatteryoperation.Object.Find(x => x.id == 2);
            this.mockbatteryoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new battery record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertBattery()
        {
            var newbattery = new battery()
            {
                id = 9,
                name = "test",
                brand_id = 3,
                cells = 4,
                c = 999,
                mass = 1
            };

            this.mockbatteryoperation.Object.Insert(newbattery);
            this.mockbatteryoperation.Verify(x => x.Insert(newbattery), Times.Once);
        }

        /// <summary>
        /// tests if a battery record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteBattery()
        {
            this.mockbatteryoperation.Object.Delete(x => x.id == 2);
            this.mockbatteryoperation.Verify(x => x.Delete(y => y.id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a battery record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateBattery()
        {
            var bat1 = new battery()
            {
                id = 2,
                name = "Wizard X220S FPV Racer Spare Part 4S 14.8V 1500mAh",
                brand_id = 1,
                cells = 4,
                c = 75,
                mass = 1170
            };
            this.mockbatteryoperation.Object.Update(bat1, x => x.id == bat1.id);
            this.mockbatteryoperation.Verify(x => x.Update(bat1, y => y.id == bat1.id));
        }

        /// <summary>
        /// tests if all build recurds are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllBuilds()
        {
            this.mockbuildoperation.Object.Find(null);
            this.mockbuildoperation.Verify(x => x.Find(null), Times.Once);
        }

        /// <summary>
        /// get a build record by id
        /// </summary>
        [Test]
        public void GetBuildById()
        {
            this.mockbuildoperation.Object.Find(x => x.build_id == 2);
            this.mockbuildoperation.Verify(x => x.Find(y => y.build_id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a new build record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertBuild()
        {
            var newbuild = new builds()
            {
                build_id = 7,
                buildname = "test build",
                brand_id = 4,
                frame_id = 3,
                motor_id = 3,
                esc_id = 2,
                pdb_id = 2,
                fc_id = 1,
                camera_id = 2,
                vtx_id = 5,
                prop_id = 5,
                battery_id = 3
            };

            this.mockbuildoperation.Object.Insert(newbuild);
            this.mockbuildoperation.Verify(x => x.Insert(newbuild), Times.Once);
        }

        /// <summary>
        /// tests if a build record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteBuild()
        {
            this.mockbuildoperation.Object.Delete(x => x.build_id == 2);
            this.mockbuildoperation.Verify(x => x.Delete(y => y.build_id == 2), Times.Once);
        }

        /// <summary>
        /// tests if a build record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateBuild()
        {
            var b1 = new builds()
            {
                build_id = 1,
                buildname = "100$ build",
                brand_id = null,
                frame_id = 1,
                motor_id = 2,
                esc_id = 2,
                pdb_id = 3,
                fc_id = 1,
                camera_id = 1,
                vtx_id = 2,
                prop_id = 1,
                battery_id = 1
            };
            this.mockbuildoperation.Object.Update(b1, x => x.build_id == b1.build_id);
            this.mockbuildoperation.Verify(x => x.Update(b1, y => y.build_id == b1.build_id));
        }

        /// <summary>
        /// tests if all frame recurds are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAllFrames()
        {
            this.mockframeoperation.Object.Find(null);
            this.mockframeoperation.Verify(x => x.Find(null), Times.Once);
        }

        // /// <summary>
        // /// tests if all recurds are returned by the Find() method if the parameter is null
        // /// </summary>
        // [Test]
        // public void GetAllFrames()
        // {
        //    var testdbcount = this.testdb.Object.Set<frames>().AsEnumerable().Count<frames>() /* -1*/; // (removed for now) 1 is subtracted because in the mock db there is an additional item for testing the update command
        //    // var asd = this.testoperation.Find(null);
        //    // List<List<string>> tmp = new List<List<string>>();
        //    // foreach (var item in asd)
        //    // {
        //    //    List<string> record = new List<string>();
        //    //    record.Add(item.id.ToString());
        //    //    record.Add(item.name);
        //    //    record.Add(item.brand_id.ToString());
        //    //    tmp.Add(record);
        //    // }
        //    var actualdbcount = this.testoperation.Find(null).Count<frames>();
        //    Assert.That(actualdbcount.Equals(testdbcount));
        // }

        /// <summary>
        /// get frame record by id
        /// </summary>
        [Test]
        public void GetFrameById()
        {
            this.mockframeoperation.Object.Find(x => x.id == 2);
            this.mockframeoperation.Verify(x => x.Find(y => y.id == 2), Times.Once);
        }

        // /// <summary>
        // /// get record by id
        // /// </summary>
        // [Test]
        // public void GetById()
        // {
        //    Assert.That(this.testoperation.Find(x => x.id == 1).FirstOrDefault().name, Is.EqualTo("Lisam LS-210"));
        // }

        /// <summary>
        /// tests if a new frame record is inserted through the ILogic interface
        /// </summary>
        [Test]
        public void InsertFrame()
        {
            var newframe = new frames()
            {
                id = 7,
                name = "fake Crusader GT2 200",
                brand_id = 4,
                size = 200,
                psize = 5,
                msize = "2205",
                armpiece = false,
                armthickness = 4,
                bplatethickness = 4,
                tplatetickness = null,
                mass = 95,
                material = "chineseium"
            };

            this.mockframeoperation.Object.Insert(newframe);
            this.mockframeoperation.Verify(x => x.Insert(newframe), Times.Once);
        }

        /// <summary>
        /// tests if a frame record is deleted (by ID) through the ILogic interface
        /// </summary>
        [Test]
        public void DeleteFrame()
        {
            this.mockframeoperation.Object.Delete(x => x.id == 6);
            this.mockframeoperation.Verify(x => x.Delete(y => y.id == 6), Times.Once);
        }

        // /// <summary>
        // /// tests if a new record is inserted with ID = 6 and than deletes it
        // /// (and if it is already inserted than deletes it before inserting too)
        // /// </summary>
        // [Test]
        // public void InsertandDeleteRecord()
        // {
        //    var newframe = new frames()
        //    {
        //        id = 7,
        //        name = "fake Crusader GT2 200",
        //        brand_id = 4,
        //        size = 200,
        //        psize = 5,
        //        msize = "2205",
        //        armpiece = false,
        //        armthickness = 4,
        //        bplatethickness = 4,
        //        tplatetickness = null,
        //        mass = 95,
        //        material = "chineseium"
        //    };
        //    var tmp = this.testoperation.Find(x => x.id == 7).DefaultIfEmpty<frames>();
        //    if (tmp.First<frames>() != null)
        //    {
        //        this.testoperation.Delete(x => x.id == 7);
        //    }
        //    int preinsertcount = this.testoperation.Find(null).Count<frames>();
        //    this.testoperation.Insert(newframe);
        //    int afterinsertcount = this.testoperation.Find(null).Count<frames>();
        //    this.testoperation.Delete(x => x.id == 7);
        //    int afterdeletecount = this.testoperation.Find(null).Count<frames>();
        //    Assert.That(afterinsertcount, Is.EqualTo(preinsertcount + 1));
        //    Assert.That(preinsertcount, Is.EqualTo(afterdeletecount));
        // }

        /// <summary>
        /// tests if a frame record is updated through the ILogic interface
        /// </summary>
        [Test]
        public void UpdateFrame()
        {
            var f1 = new frames()
            {
                id = 6,
                name = "testframe",
                brand_id = 15,
                size = 210,
                psize = 5,
                msize = "1806,2204",
                armpiece = false,
                armthickness = 3,
                bplatethickness = 3,
                tplatetickness = 2,
                mass = 110,
                material = "3k carbon"
            };
            this.mockframeoperation.Object.Update(f1, x => x.id == f1.id);
            this.mockframeoperation.Verify(x => x.Update(f1, y => y.id == f1.id));
        }

        // /// <summary>
        // /// Modifies a record for testing (and restores it afterwards)
        // /// </summary>
        // [Test]
        // public void ModifyRecord()
        // {
        //    var f1 = new frames()
        //    {
        //        id = 6,
        //        name = "testframe",
        //        brand_id = 15,
        //        size = 210,
        //        psize = 5,
        //        msize = "1806,2204",
        //        armpiece = false,
        //        armthickness = 3,
        //        bplatethickness = 3,
        //        tplatetickness = 2,
        //        mass = 110,
        //        material = "3k carbon"
        //    };
        //    var f2 = new frames()
        //    {
        //        id = 6,
        //        name = "testframe",
        //        brand_id = 15,
        //        size = 210,
        //        psize = 5,
        //        msize = "1806,2204",
        //        armpiece = false,
        //        armthickness = 3,
        //        bplatethickness = 3,
        //        tplatetickness = 2,
        //        mass = 100,
        //        material = "3k carbon"
        //    };
        //    // var oldmass = this.testoperation.Find(x => x.id == 6).First<frames>().mass; // only for debugging
        //    this.testoperation.Update(f1, x => x.id == f1.id);
        //    var newmass = this.testoperation.Find(x => x.id == 6).First<frames>().mass;
        //    Assert.That(newmass, Is.EqualTo(110));
        //    this.testoperation.Update(f2, x => x.id == f2.id);
        // }

        /// <summary>
        /// Tests if ComplexOperations.Get_build_mass method returns the correct values.
        /// </summary>
        [Test]
        public void Get_Build_Mass()
        {
            ComplexOperations complex = new ComplexOperations();
            complex.Get_Build_Mass(this.queryall.Builds.Result);
            List<List<string>> result = ParamCollector.QueryResult;
            List<string> mass = new List<string>();
            for (int i = 0; i < result.Count; i++)
            {
                mass.Add(result[i].ElementAt<string>(2));
            }

            Assert.That(mass[0], Is.EqualTo("443"));
            Assert.That(mass[1], Is.EqualTo("446"));
            Assert.That(mass[2], Is.EqualTo("582"));
            Assert.That(mass[3], Is.EqualTo("448"));
            Assert.That(mass[4], Is.EqualTo("654"));
            Assert.That(mass[5], Is.EqualTo("777"));
        }

        /// <summary>
        /// Tests if ComplexOperations.List_Build_Twr method returns the correct values.
        /// </summary>
        [Test]
        public void List_Build_Twr()
        {
            ComplexOperations complex = new ComplexOperations();
            complex.List_Build_Twr(this.queryall.Builds.Result);
            List<List<string>> result = ParamCollector.QueryResult;
            List<string> twr = new List<string>();
            for (int i = 0; i < result.Count; i++)
            {
                twr.Add(result[i].ElementAt<string>(2));
            }

            Assert.That(twr[0], Is.EqualTo("8,57787810383747"));
            Assert.That(twr[1], Is.EqualTo("10,5829596412556"));
            Assert.That(twr[2], Is.EqualTo("7,56013745704467"));
            Assert.That(twr[3], Is.EqualTo("10,5357142857143"));
            Assert.That(twr[4], Is.EqualTo("9,0519877675841"));
            Assert.That(twr[5], Is.EqualTo("12,1492921492921"));
        }

        /// <summary>
        /// Tests if ComplexOperations.List_Build_Twr method returns the correct values.
        /// </summary>
        [Test]
        public void List_Brand_Products()
        {
            ComplexOperations complex = new ComplexOperations();
            complex.List_Brand_Products(this.queryall.Brands.Result);
            List<List<string>> result = ParamCollector.QueryResult;
            List<string> products = new List<string>();
            for (int i = 0; i < result.Count; i++)
            {
                products.Add(result[i].ElementAt<string>(2));
            }

            Assert.That(products[0], Is.EqualTo("10"));
            Assert.That(products[1], Is.EqualTo("6"));
            Assert.That(products[2], Is.EqualTo("3"));
            Assert.That(products[3], Is.EqualTo("4"));
            Assert.That(products[4], Is.EqualTo("1"));
            Assert.That(products[5], Is.EqualTo("3"));
            Assert.That(products[6], Is.EqualTo("3"));
            Assert.That(products[7], Is.EqualTo("1"));
            Assert.That(products[8], Is.EqualTo("4"));
            Assert.That(products[9], Is.EqualTo("1"));
            Assert.That(products[10], Is.EqualTo("2"));
            Assert.That(products[11], Is.EqualTo("1"));
            Assert.That(products[12], Is.EqualTo("2"));
            Assert.That(products[13], Is.EqualTo("5"));
            Assert.That(products[14], Is.EqualTo("2"));
        }

        /// <summary>
        /// implementation of the IDisposable interface
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// implementation of the IDisposable interface
        /// </summary>
        /// <param name="disposing">????</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // this.testbuildoperation.Dispose();
                // this.testoperation.Dispose();
                this.testdb.Dispose();
                this.queryall.Dispose();
            }
        }
    }
}
