﻿// <copyright file="MockDbSetFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;

    /// <summary>
    /// Creates a mock DbSet from the specified data.
    /// </summary>
    public static class MockDbSetFactory
    {
        /// <summary>
        /// Creates a mock DbSet from the specified data.
        /// </summary>
        /// <typeparam name="T">table</typeparam>
        /// <param name="data">list of T type data to put in the dbset</param>
        /// <returns>dbset</returns>
        public static Mock<DbSet<T>> Create<T>(IEnumerable<T> data)
            where T : class
        {
            var queryable = data.AsQueryable();
            var mock = new Mock<DbSet<T>>();
            mock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            mock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            mock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            mock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            return mock;
        }
    }
}
