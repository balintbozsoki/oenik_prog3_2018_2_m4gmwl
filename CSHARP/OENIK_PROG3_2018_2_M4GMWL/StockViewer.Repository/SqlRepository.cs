﻿// <copyright file="SqlRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using StockViewer.Repository.Interfaces;

    /// <summary>
    /// Class for all SQL based operations
    /// </summary>
    /// <typeparam name="T">object type</typeparam>
    public class SqlRepository<T> : IRepository<T>
        where T : class
    {
        private DbSet<T> dbSet;
        private DbContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlRepository{T}"/> class.
        /// </summary>
        /// <param name="db">DBSet to work with</param>
        public SqlRepository(DbContext db)
        {
            if (db == null)
            {
                throw new ArgumentNullException("db");
            }

            this.db = db;
            this.dbSet = db.Set<T>();
        }

        /// <summary>
        /// Gets or sets dbset
        /// </summary>
        private DbSet<T> DB_set
        {
            get
            {
                return this.dbSet;
            }

            set
            {
                this.dbSet = value;
            }
        }

        /// <inheritdoc/>
        public void Delete(Expression<Func<T, bool>> whereCondition)
        {
            IEnumerable<T> entities = this.Find(whereCondition);
            foreach (T entity in entities)
            {
                if (this.db.Entry(entity).State == EntityState.Detached)
                {
                    this.dbSet.Attach(entity);
                }

                this.dbSet.Remove(entity);
            }

            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<T> Find(Expression<Func<T, bool>> whereCondition)
        {
            if (whereCondition is null)
            {
                return this.dbSet.AsQueryable<T>();
            }
            else
            {
                return this.dbSet.Where(whereCondition).AsQueryable<T>();
            }
        }

        /// <inheritdoc/>
        public void Insert(T entity)
        {
            this.dbSet.Add(entity);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(T entity, Expression<Func<T, bool>> whereCondition)
        {
            this.Delete(whereCondition);
            this.Insert(entity);
        }
    }
}
