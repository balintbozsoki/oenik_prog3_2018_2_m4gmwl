﻿INSERT INTO brands VALUES(1,'Eachine','China');
INSERT INTO brands VALUES(2,'RacerStar','Japan');
INSERT INTO brands VALUES(3,'Dinogy','USA');
INSERT INTO brands VALUES(4,'Diatone','USA');
INSERT INTO brands VALUES(5,'Dal','USA');
INSERT INTO brands VALUES(6,'Matek','China');
INSERT INTO brands VALUES(7,'Runcam','USA');
INSERT INTO brands VALUES(8,'SP Racing','USA');
INSERT INTO brands VALUES(9,'Realacc','China');
INSERT INTO brands VALUES(10,'Sunnysky','Japan');
INSERT INTO brands VALUES(11,'Kingkong','USA');
INSERT INTO brands VALUES(12,'Zop Power','China');
INSERT INTO brands VALUES(13,'Foxeer','USA');
INSERT INTO brands VALUES(14,'EMAX','USA');
INSERT INTO brands VALUES(15,'noname','N/A');

INSERT INTO battery VALUES(1,'Giant Power Mega Graphene 2.0 14.8V 1550mAh',3,4,75,160);
INSERT INTO battery VALUES(2,'Wizard X220S FPV Racer Spare Part 4S 14.8V 1500mAh',1,4,75,177);
INSERT INTO battery VALUES(3,'Giant Power 1500mAh 14.8V',3,4,65,160);
INSERT INTO battery VALUES(4,'11.1V 1500mAh',12,3,40,113);
INSERT INTO battery VALUES(5,'Giant Power Mega Graphene 18.5V 1350mAh',3,5,75,190);

INSERT INTO Cams VALUES(1,'CCD 110 Degree 2.8mm Lens Mini FPV camera',1,'1000TVL',1,1,10);
INSERT INTO Cams VALUES(2,'Swift 2',7,'600TVL',1,0,14);
INSERT INTO Cams VALUES(3,'Monster',13,'1200TVL',1,1,15);
INSERT INTO Cams VALUES(4,'Split 2',7,'HD',1,1,21);
INSERT INTO Cams VALUES(5,'Split',7,'HD',1,1,21);

INSERT INTO FC VALUES(1,'F3 Deluxe',8,0,0,0,0,1,1,1,'F3','Cleanflight,Betaflight',6);
INSERT INTO FC VALUES(2,'F405 AIO',6,0,1,0,1,1,1,1,'F405','Cleanflight,Betaflight',12);
INSERT INTO FC VALUES(3,'TattooF4S',2,1,1,0,1,0,0,0,'F4','Cleanflight,Betaflight',22);
INSERT INTO FC VALUES(4,'Magnum',14,1,0,1,1,1,1,1,'F4','Cleanflight,Betaflight',22);
INSERT INTO FC VALUES(5,'Omnibus F4',1,0,0,0,0,0,0,0,'F4','Betaflight,dRonin',6);

INSERT INTO ESC VALUES(1,'RS20A',2,null,'2-4S',20,'10s/m',25,8);
INSERT INTO ESC VALUES(2,'RS30A V2',2,null,'2-4S',30,'10s/m',35,6);
INSERT INTO ESC VALUES(3,'Simonk Series',14,null,'2-6S',40,'10s/m',50,41);
INSERT INTO ESC VALUES(4,'TattooF4S',2,3,'2-4S',30,'10s/m',35,null);
INSERT INTO ESC VALUES(5,'Magnum',14,4,'2-4S',30,'10s/m',35,null);
INSERT INTO ESC VALUES(6,'Eachine BLHELI 4 in 1 ESC',1,null,'2-5S',30,'10s/m',35,28);

INSERT INTO PDB VALUES(1,'F405 AIO',6,2,120,1,1,4,null);
INSERT INTO PDB VALUES(2,'PDB-XPW',9,null,140,1,1,4,7);
INSERT INTO PDB VALUES(3,'PDB-XT60',6,null,100,1,1,6,8);
INSERT INTO PDB VALUES(4,'Diatone V8.3 LC Filter Power PDB Board Low Ripple Current',4,null,80,1,1,4,6);
INSERT INTO PDB VALUES(5,'HUBOSD ECO',9,null,140,1,1,4,9);

INSERT INTO VTX VALUES(1,'Magnum',4,14,48,'5.8ghz','200mw',null);
INSERT INTO VTX VALUES(2,'TX801',null,1,72,'5.8ghz','600mw',7);
INSERT INTO VTX VALUES(3,'VTX01 Super Mini',null,1,40,'5.8ghz','25mw',3);
INSERT INTO VTX VALUES(4,'GX210',null,9,40,'5.8ghz','200mw',6);
INSERT INTO VTX VALUES(5,'TM200',null,13,40,'5.8ghz','200mw',10);

INSERT INTO frames VALUES(1,'Lisam LS-210',15,210,5,'1806,2204',0,3,3,2,100,'3k carbon');
INSERT INTO frames VALUES(2,'Real1',9,220,5,'2205,2306',1,4,2,2,104,'3k carbon,aluminum');
INSERT INTO frames VALUES(3,'Crusader GT2 200',4,200,5,'2205',0,5,5,null,95,'3k carbon');
INSERT INTO frames VALUES(4,'Wizard x220S',1,220,5,'2205',1,2,2,2,131,'carbon,glass fiber');
INSERT INTO frames VALUES(5,'Martian II',15,220,5,'1806,2204,2205,2206',0,4,2,2,130,'3k carbon');

INSERT INTO prop VALUES(1,'Kingkong 5045',11,'5x4.5',4);
INSERT INTO prop VALUES(2,'X220S 5051 3 Blade Propeller',1,5,5);
INSERT INTO prop VALUES(3,'Cyclone T5046C',5,5,5);
INSERT INTO prop VALUES(4,'Kingkong 6040',11,6,6);
INSERT INTO prop VALUES(5,'5040 Super Durable',4,5,4);

INSERT INTO motors VALUES(1,'BR2205',2,2205,2300,27,950,28,'2-4S');
INSERT INTO motors VALUES(2,'MN 2206',1,2206,2300,28,1100,28,'3-5S');
INSERT INTO motors VALUES(3,'EMAX 2205S',14,2205,2300,28,1180,29,'3-4S');
INSERT INTO motors VALUES(4,'X2814',10,2814,900,60,2360,108,'3-4S');
INSERT INTO motors VALUES(5,'BR2406S',2,2406,2600,42,1480,35,'2-4S');

INSERT INTO builds VALUES(1,'100$ build',null,1,1,2,3,1,1,2,1,1);
INSERT INTO builds VALUES(2,'racer',null,2,3,5,5,4,3,1,3,1);
INSERT INTO builds VALUES(3,'Wizard X220S',1,4,2,6,2,5,1,2,2,2);
INSERT INTO builds VALUES(4,'Crusader GT2',4,3,3,2,2,1,2,5,5,3);
INSERT INTO builds VALUES(5,'freestyle build',null,5,5,3,1,2,5,2,3,1);
INSERT INTO builds VALUES(6,'buildbuild',null,1,4,1,2,1,2,5,5,3);