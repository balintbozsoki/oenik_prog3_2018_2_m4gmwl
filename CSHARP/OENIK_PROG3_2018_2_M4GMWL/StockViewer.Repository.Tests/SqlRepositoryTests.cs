﻿// <copyright file="SqlRepositoryTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Repository.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using StockViewer.Data;
    using StockViewer.Repository;
    using StockViewer.Repository.Interfaces;

    /// <summary>
    /// Test class for the SqlRepository class.
    /// </summary>
    [TestFixture]
    public sealed class SqlRepositoryTests
    {
        private Mock<MultirotorDBEntities> testdb;

        // private Mock<DbSet<frames>> testdbset;
        private SqlRepository<frames> repo;

        /// <summary>
        /// Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.testdb = new Mock<MultirotorDBEntities>();
            List<frames> myframes = new List<frames>()
            {
                new frames()
                {
                    id = 1,
                    name = "Lisam LS-210",
                    brand_id = 15,
                    size = 210,
                    psize = 5,
                    msize = "1806,2204",
                    armpiece = false,
                    armthickness = 3,
                    bplatethickness = 3,
                    tplatetickness = 2,
                    mass = 100,
                    material = "3k carbon"
                },
                new frames()
                {
                    id = 2,
                    name = "Real1",
                    brand_id = 9,
                    size = 220,
                    psize = 5,
                    msize = "2205,2306",
                    armpiece = true,
                    armthickness = 4,
                    bplatethickness = 2,
                    tplatetickness = 2,
                    mass = 104,
                    material = "carbon,aluminum"
                },
                new frames()
                {
                    id = 3,
                    name = "Crusader GT2 200",
                    brand_id = 4,
                    size = 200,
                    psize = 5,
                    msize = "2205",
                    armpiece = false,
                    armthickness = 5,
                    bplatethickness = 5,
                    tplatetickness = null,
                    mass = 95,
                    material = "3k carbon"
                },
                new frames()
                {
                    id = 4,
                    name = "Wizard x220S",
                    brand_id = 1,
                    size = 220,
                    psize = 5,
                    msize = "2205",
                    armpiece = true,
                    armthickness = 2,
                    bplatethickness = 2,
                    tplatetickness = 2,
                    mass = 130,
                    material = "carbon,glass fiber"
                },
                new frames()
                {
                    id = 5,
                    name = "LMartian II",
                    brand_id = 15,
                    size = 220,
                    psize = 5,
                    msize = "1806,2204,2205,2206",
                    armpiece = false,
                    armthickness = 4,
                    bplatethickness = 2,
                    tplatetickness = 2,
                    mass = 130,
                    material = "3k carbon"
                }
            };
            var testdbset = MockDbSetFactory.Create(myframes);
            this.testdb.Setup(o => o.Set<frames>()).Returns(() => testdbset.Object);
            this.repo = new SqlRepository<frames>(this.testdb.Object);
        }

        /// <summary>
        /// tests if all recurds are returned by the Find() method if the parameter is null
        /// </summary>
        [Test]
        public void GetAll()
        {
            Assert.That(this.repo.Find(null), Is.EqualTo(this.testdb.Object.Set<frames>()));
        }

        /// <summary>
        /// get record by id
        /// </summary>
        [Test]
        public void GetById()
        {
            Assert.That(this.repo.Find(x => x.id == 1).FirstOrDefault().name, Is.EqualTo("Lisam LS-210"));
        }

        ///// <summary>
        ///// tests if a new record is inserted
        ///// </summary>
        // [Test]
        // public void InsertRecord()
        // {
        //    var newframe = new frames()
        //    {
        //        id = 6,
        //        name = "fake Crusader GT2 200",
        //        brand_id = 4,
        //        size = 200,
        //        psize = 5,
        //        msize = "2205",
        //        armpiece = false,
        //        armthickness = 4,
        //        bplatethickness = 4,
        //        tplatetickness = null,
        //        mass = 95,
        //        material = "chineseium"
        //    };
        //    int preinsertcount = this.repo.Find(null).Count<frames>();
        //    this.repo.Insert(newframe);
        //    int afterinsertcount = this.repo.Find(null).Count<frames>();
        //    Assert.That(preinsertcount, Is.EqualTo(preinsertcount + 1));

        // // Assert.That(this.repo.Find(x => x.id == 6).FirstOrDefault().name, Is.EqualTo("fake Crusader GT2 200"));
        // }

        ///// <summary>
        ///// tests if a record can be deleted
        ///// </summary>
        // [Test]
        // public void DeleteRecord()
        // {
        //    var f1 = new frames()
        //    {
        //        id = 1,
        //        name = "Lisam LS-210",
        //        brand_id = 15,
        //        size = 210,
        //        psize = 5,
        //        msize = "1806,2204",
        //        armpiece = false,
        //        armthickness = 3,
        //        bplatethickness = 3,
        //        tplatetickness = 2,
        //        mass = 100,
        //        material = "3k carbon"
        //    };
        //    this.repo.Delete(x => x.id == 1);

        // // var asdasdasd = this.repo.Find(x => x.id == 15).Count<frames>();
        //    Assert.That(this.repo.Find(null).First<frames>(), Is.Not.EqualTo(f1));
        // }
    }
}
