﻿// <copyright file="ParamInputDialog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Creates a dialog in the console that lets the user input parameters.
    /// </summary>
    public static class ParamInputDialog
    {
        /// <summary>
        /// Creates a dialog in the console that lets the user input parameters.
        /// </summary>
        /// <param name="query">Query text.</param>
        /// <param name="autofill">parameter to modify</param>
        /// <returns>Parameter as string.</returns>
        public static string GetParam(string query, string autofill)
        {
            Console.Clear();
            Console.WriteLine(query);
            if (autofill != string.Empty || autofill != null)
            {
                Console.Write(autofill);
            }

            return Console.ReadLine();
        }
    }
}
