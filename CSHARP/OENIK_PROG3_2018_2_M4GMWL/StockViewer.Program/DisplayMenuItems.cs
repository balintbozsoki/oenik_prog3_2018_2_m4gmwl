﻿// <copyright file="DisplayMenuItems.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using StockViewer.Logic;

    /// <summary>
    /// Draws menu items on the console
    /// </summary>
    public class DisplayMenuItems
    {
        /// <summary>
        /// Reads the pressed key and calls the <see cref="WriteMenuToConsole(XDocument, string, int)"/> accordingly until the menu is exited.
        /// </summary>
        /// <param name="items">XDocument containing the menu items.</param>
        /// <param name="parentname">Name of the parent node in the items XDocument whose direct descendants are the menu items we want to display.</param>
        /// <param name="selected">The menu item selected by the user.</param>
        public void RunMenu(XDocument items, string parentname, int selected)
        {
            bool runs = true;
            string selectedname = parentname;
            string prev_selectedname = null;
            this.WriteMenuToConsole(items, parentname, selected);
            ConsoleKey key = Console.ReadKey().Key;
            while (runs)
            {
                switch (key)
                {
                    case ConsoleKey.DownArrow:
                        selected++;
                        selectedname = this.WriteMenuToConsole(items, parentname, selected);
                        break;
                    case ConsoleKey.UpArrow:
                        selected--;
                        selectedname = this.WriteMenuToConsole(items, parentname, selected);
                        break;
                    case ConsoleKey.Enter:
                        parentname = selectedname;
                        prev_selectedname = selectedname;
                        selectedname = this.WriteMenuToConsole(items, selectedname, 1);
                        if (selectedname.Substring(0, 4) == "Call")
                        {
                            this.GetArguments(selectedname.Substring(6));
                            CallMethod.Call(selectedname.Substring(6));
                            List<List<string>> result = ParamCollector.QueryResult;
                            for (int i = 0; i < result.Count; i++)
                            {
                                string record = string.Empty;
                                foreach (var item in result[i])
                                {
                                    record += item + "\t";
                                }

                                Console.WriteLine(record);
                            }

                            Console.ReadLine();
                            this.WriteMenuToConsole(items, items.Descendants(parentname).First().Parent.Name.ToString(), 1);
                            parentname = items.Descendants(parentname).First().Parent.Name.ToString();
                            selected = 1;
                        }

                        selectedname = prev_selectedname;
                        selected = 1;
                        break;
                    case ConsoleKey.Backspace:
                        if (parentname != "Root")
                        {
                            this.WriteMenuToConsole(items, items.Descendants(parentname).First().Parent.Name.ToString(), 1);
                            parentname = items.Descendants(parentname).First().Parent.Name.ToString();
                            selected = 1;
                        }
                        else
                        {
                            key = ConsoleKey.Escape;
                        }

                        break;
                }

                if (key == ConsoleKey.Escape)
                {
                    break;
                }
                else
                {
                    key = Console.ReadKey().Key;
                }
            }
        }

        /// <summary>
        /// <see cref="DisplayMenuItems"/>
        /// </summary>
        /// <param name="menuitems">XDocument containing the menu items.</param>
        /// <param name="parentname">Name of the parent node in the items XDocument whose direct descendants are the menu items we want to display.</param>
        /// <param name="selected">The menu item selected by the user.</param>
        /// <returns>Returns the name of the selected item for calling this method to display the submenus if needed.</returns>
        private string WriteMenuToConsole(XDocument menuitems, string parentname, int selected)
        {
            Console.Clear();
            string header = null;
            int index = 0;
            string selectedname = parentname;
            foreach (XElement item in menuitems.Descendants(parentname).Elements())
            {
                if (item.Name == "Header")
                {
                    header = item?.Value;
                    break;
                }
            }

            if (header != null)
            {
                Console.Write(header);
            }

            foreach (XElement item in menuitems.Descendants(parentname).Elements())
            {
                if (item.Name == "Call")
                {
                    return "Call: " + item?.Value;
                }

                if (item.Name == "Submenu")
                {
                    XDocument menu = XmlHandler.ReadXml(item.Value);
                    DisplayMenuItems submenu = new DisplayMenuItems();
                    submenu.RunMenu(menu, "Root", 0);
                }

                if (item.Name == "Table")
                {
                    ParamCollector.Add(item.Name.ToString(), item.Value);
                }

                if (index == selected)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(item.Element("Text")?.Value);
                    Console.ForegroundColor = ConsoleColor.White;
                    selectedname = item.Name.ToString();
                }
                else
                {
                    Console.WriteLine(item.Element("Text")?.Value);
                }

                index++;
            }

            return selectedname;
        }

        private void GetArguments(string command)
        {
            switch (command)
            {
                case "Find_By_ID":
                    ParamCollector.Add("ID", ParamInputDialog.GetParam("Please type the ID of the record you want to find.", null));
                    break;
                case "Insert":
                    List<Param> properties = PropertyExtractor.ExtractProperties(ParamCollector.Find("Table").Value.ToString());
                    foreach (var item in properties)
                    {
                        ParamCollector.Add(item.Name, ParamInputDialog.GetParam(item.Name + " (" + item.Value + ")", null));
                    }

                    foreach (var item in ParamCollector.NewRecords)
                    {
                        item.Value = false;
                    }

                    ParamCollector.AddRecordType(ParamCollector.Find("Table").Value.ToString(), true);

                    break;
                case "Update":
                    List<Param> propertiees = PropertyExtractor.ExtractProperties(ParamCollector.Find("Table").Value.ToString());
                    foreach (var item in propertiees)
                    {
                        ParamCollector.Add(item.Name, ParamInputDialog.GetParam(item.Name + " (" + item.Value + ")", null));
                    }

                    foreach (var item in ParamCollector.NewRecords)
                    {
                        item.Value = false;
                    }

                    ParamCollector.AddRecordType(ParamCollector.Find("Table").Value.ToString(), true);
                    break;
                case "Delete":
                    ParamCollector.Add("ID", ParamInputDialog.GetParam("Please type the ID of the record you want to delete.", null));
                    break;
            }
        }
    }
}
