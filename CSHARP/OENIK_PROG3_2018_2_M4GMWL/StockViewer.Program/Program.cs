﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using StockViewer.Logic;
    using StockViewer.Logic.Interfaces;

    /// <summary>
    /// Creates the instances and calls the methods needed to run the program
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main() method
        /// </summary>
        private static void Main()
        {
            ParamCollector.AddRecordType("battery", false);
            ParamCollector.AddRecordType("brands", false);
            ParamCollector.AddRecordType("builds", false);
            ParamCollector.AddRecordType("Cams", false);
            ParamCollector.AddRecordType("ESC", false);
            ParamCollector.AddRecordType("FC", false);
            ParamCollector.AddRecordType("frames", false);
            ParamCollector.AddRecordType("motors", false);
            ParamCollector.AddRecordType("PDB", false);
            ParamCollector.AddRecordType("prop", false);
            ParamCollector.AddRecordType("VTX", false);
            XDocument mainMenu = XmlHandler.ReadXml("MainMenu.xml");
            DisplayMenuItems main = new DisplayMenuItems();
            main.RunMenu(mainMenu, "Root", 0);
            Console.WriteLine("Menu exited.");
            Console.ReadLine();
        }
    }
}
