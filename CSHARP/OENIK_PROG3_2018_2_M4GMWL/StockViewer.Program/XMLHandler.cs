﻿// <copyright file="XMLHandler.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StockViewer.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Reads the XML file on the given URL and returns an XDocument or writes it.
    /// </summary>
    public static class XmlHandler
    {
        /// <summary>
        /// Reads the XML file on the given URL..
        /// </summary>
        /// <param name="url">File path or link of the XML file to read.</param>
        /// <returns>Returns an XDocument if the URL is valid.</returns>
        public static XDocument ReadXml(string url)
        {
            XDocument document = new XDocument();
            if (url == null)
            {
                throw new Exception("The file path can't be null.");
            }
            else
            {
                try
                {
                    document = XDocument.Load(url);
                }
                catch (Exception)
                {
                    throw new Exception("File not found.");
                }
            }

            return document;
        }
    }
}
