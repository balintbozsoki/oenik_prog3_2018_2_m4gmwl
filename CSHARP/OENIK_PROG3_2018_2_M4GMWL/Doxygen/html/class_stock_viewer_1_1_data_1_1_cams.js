var class_stock_viewer_1_1_data_1_1_cams =
[
    [ "Cams", "class_stock_viewer_1_1_data_1_1_cams.html#a5bd6897d851c13bf5fe625a2fe702552", null ],
    [ "brand_id", "class_stock_viewer_1_1_data_1_1_cams.html#ae317aa2cbdd4a84d13550abc4a79f0b9", null ],
    [ "brands", "class_stock_viewer_1_1_data_1_1_cams.html#a5a6a11561fc0fc89a7819a6bcad23fb7", null ],
    [ "builds", "class_stock_viewer_1_1_data_1_1_cams.html#a2678780649f65f802cedba27ec58c2ab", null ],
    [ "id", "class_stock_viewer_1_1_data_1_1_cams.html#a5229ad4d4d91d397ffd757e27effc62f", null ],
    [ "mass", "class_stock_viewer_1_1_data_1_1_cams.html#ab49c9a72fa52cb9d7e3fc437d186acfc", null ],
    [ "name", "class_stock_viewer_1_1_data_1_1_cams.html#a18beb6725ea2bffed56ac76392ad9faf", null ],
    [ "ntsc", "class_stock_viewer_1_1_data_1_1_cams.html#a689f66924b973278d7077d622017e8bf", null ],
    [ "pal", "class_stock_viewer_1_1_data_1_1_cams.html#a472862383bbe6effa4590b48e7738774", null ],
    [ "res", "class_stock_viewer_1_1_data_1_1_cams.html#acf65737c5527d298470cabd216c5a5a4", null ]
];