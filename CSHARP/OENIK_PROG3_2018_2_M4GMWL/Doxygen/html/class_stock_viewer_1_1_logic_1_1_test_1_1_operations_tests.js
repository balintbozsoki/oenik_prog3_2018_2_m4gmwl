var class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests =
[
    [ "Dispose", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a077a233a92c7898821736886a2dc6e17", null ],
    [ "Dispose", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a378254b60ad8f89843a283543c2c04fe", null ],
    [ "Get_Build_Mass", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#abbeafdbaa3b8f9d12ab72038485cf28d", null ],
    [ "GetAll", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a09770f9756a3d7d6781e78f212902e1a", null ],
    [ "GetById", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a39f6f150ae1a37c1a98c435ef4524f19", null ],
    [ "InsertandDeleteRecord", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a5ca539f17c90e521670d649b7e7b7a7f", null ],
    [ "List_Brand_Products", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a3c54fb1a17120fe8d552ce035fd39d10", null ],
    [ "List_Build_Twr", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a9309cf3a5d342289ccfa9b60a4cbe979", null ],
    [ "ModifyRecord", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a7a4b2a3ee4fd2444495f55fa4a766610", null ],
    [ "Setup", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html#a3e074b111729b0ab4fe2126c0a8c0661", null ]
];