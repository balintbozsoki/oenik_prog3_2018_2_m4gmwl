var class_stock_viewer_1_1_data_1_1frames =
[
    [ "frames", "class_stock_viewer_1_1_data_1_1frames.html#abbadb15cfc80169d6c42b8a6404c1db5", null ],
    [ "armpiece", "class_stock_viewer_1_1_data_1_1frames.html#ad884ab359db362b53931335b840149d5", null ],
    [ "armthickness", "class_stock_viewer_1_1_data_1_1frames.html#ab3ced434a94ccdf56d36360dd319b9dd", null ],
    [ "bplatethickness", "class_stock_viewer_1_1_data_1_1frames.html#a2baf4fed5d8332eb504722d34b06f705", null ],
    [ "brand_id", "class_stock_viewer_1_1_data_1_1frames.html#a9a8b6dd52acd9f38d32c3f15b558e7ae", null ],
    [ "brands", "class_stock_viewer_1_1_data_1_1frames.html#a8567771c61ac69cf8c6849b922a6598c", null ],
    [ "builds", "class_stock_viewer_1_1_data_1_1frames.html#a35dc7870914ffc138b3a622bf259a546", null ],
    [ "id", "class_stock_viewer_1_1_data_1_1frames.html#affe8445d2be68cb3cd6e2beb91fdb58f", null ],
    [ "mass", "class_stock_viewer_1_1_data_1_1frames.html#a3f013ecbee03df6613024de888726809", null ],
    [ "material", "class_stock_viewer_1_1_data_1_1frames.html#afe1bdb031a97461c4b7674f2bcb64c06", null ],
    [ "msize", "class_stock_viewer_1_1_data_1_1frames.html#a46cae6488fa9504aa2ba05ae4541d5d1", null ],
    [ "name", "class_stock_viewer_1_1_data_1_1frames.html#a18c8edf28988f0185c2e992e2e76168d", null ],
    [ "psize", "class_stock_viewer_1_1_data_1_1frames.html#adc8b69210bf642d8f506e786020e9af0", null ],
    [ "size", "class_stock_viewer_1_1_data_1_1frames.html#aaf222b847a06eab1f1f144424cf313c4", null ],
    [ "tplatetickness", "class_stock_viewer_1_1_data_1_1frames.html#a9d849c2f13a1674d6697bff47d2d75fe", null ]
];