var class_stock_viewer_1_1_logic_1_1_param_collector =
[
    [ "Add", "class_stock_viewer_1_1_logic_1_1_param_collector.html#a47c58a4a8de77a1f40a731c05fd6a24f", null ],
    [ "AddRecordType", "class_stock_viewer_1_1_logic_1_1_param_collector.html#a0ca38101a3699357561f2214c7c89405", null ],
    [ "Clear", "class_stock_viewer_1_1_logic_1_1_param_collector.html#a0fb8e05707ad66f507ee6d3ef23b6fb4", null ],
    [ "Find", "class_stock_viewer_1_1_logic_1_1_param_collector.html#a5c585d594b2acefdde9a06a2ed900437", null ],
    [ "FindRecord", "class_stock_viewer_1_1_logic_1_1_param_collector.html#a0dbf301c0e448f028fb960962439fb41", null ],
    [ "ToBoolean", "class_stock_viewer_1_1_logic_1_1_param_collector.html#ac90122d751d3d8b9281e6016ce71160a", null ],
    [ "NewRecords", "class_stock_viewer_1_1_logic_1_1_param_collector.html#a7f67853615be1b3951a7a1578fb5617e", null ],
    [ "Parameters", "class_stock_viewer_1_1_logic_1_1_param_collector.html#ae708db3f73cfe76759e4d7ddbf5941d0", null ],
    [ "QueryResult", "class_stock_viewer_1_1_logic_1_1_param_collector.html#af25f82d14292d65507f6ca5fde9c3336", null ]
];