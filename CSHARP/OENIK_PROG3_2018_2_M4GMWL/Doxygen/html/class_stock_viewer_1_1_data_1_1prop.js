var class_stock_viewer_1_1_data_1_1prop =
[
    [ "prop", "class_stock_viewer_1_1_data_1_1prop.html#a1cda6aebb09c5ca59f08735d88c57052", null ],
    [ "brand_id", "class_stock_viewer_1_1_data_1_1prop.html#a37db897e3d09926a99cfbec9691a486a", null ],
    [ "brands", "class_stock_viewer_1_1_data_1_1prop.html#ac67fc661b2d973949910cd989a352904", null ],
    [ "builds", "class_stock_viewer_1_1_data_1_1prop.html#a13fa4e0ae718b47b7175dbb45450268d", null ],
    [ "id", "class_stock_viewer_1_1_data_1_1prop.html#a3e54dc8a4bcb4f96468866984453ab3b", null ],
    [ "mass", "class_stock_viewer_1_1_data_1_1prop.html#a46301777093042f69ece21e37d46edd8", null ],
    [ "name", "class_stock_viewer_1_1_data_1_1prop.html#a8b643cf6b5e9327006939385fe60f8e5", null ],
    [ "size", "class_stock_viewer_1_1_data_1_1prop.html#ade2a59a399649ee927fcb4d134697790", null ]
];