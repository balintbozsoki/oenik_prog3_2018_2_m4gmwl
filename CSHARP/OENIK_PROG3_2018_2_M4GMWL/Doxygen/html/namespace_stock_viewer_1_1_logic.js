var namespace_stock_viewer_1_1_logic =
[
    [ "Interfaces", "namespace_stock_viewer_1_1_logic_1_1_interfaces.html", "namespace_stock_viewer_1_1_logic_1_1_interfaces" ],
    [ "Test", "namespace_stock_viewer_1_1_logic_1_1_test.html", "namespace_stock_viewer_1_1_logic_1_1_test" ],
    [ "Tests", "namespace_stock_viewer_1_1_logic_1_1_tests.html", "namespace_stock_viewer_1_1_logic_1_1_tests" ],
    [ "CallMethod", "class_stock_viewer_1_1_logic_1_1_call_method.html", "class_stock_viewer_1_1_logic_1_1_call_method" ],
    [ "ComplexOperations", "class_stock_viewer_1_1_logic_1_1_complex_operations.html", "class_stock_viewer_1_1_logic_1_1_complex_operations" ],
    [ "CreateNewRecord", "class_stock_viewer_1_1_logic_1_1_create_new_record.html", "class_stock_viewer_1_1_logic_1_1_create_new_record" ],
    [ "GetTheJoke", "class_stock_viewer_1_1_logic_1_1_get_the_joke.html", "class_stock_viewer_1_1_logic_1_1_get_the_joke" ],
    [ "Operations", "class_stock_viewer_1_1_logic_1_1_operations.html", "class_stock_viewer_1_1_logic_1_1_operations" ],
    [ "Param", "class_stock_viewer_1_1_logic_1_1_param.html", "class_stock_viewer_1_1_logic_1_1_param" ],
    [ "ParamCollector", "class_stock_viewer_1_1_logic_1_1_param_collector.html", "class_stock_viewer_1_1_logic_1_1_param_collector" ],
    [ "PropertyExtractor", "class_stock_viewer_1_1_logic_1_1_property_extractor.html", "class_stock_viewer_1_1_logic_1_1_property_extractor" ],
    [ "QueryAllTables", "class_stock_viewer_1_1_logic_1_1_query_all_tables.html", "class_stock_viewer_1_1_logic_1_1_query_all_tables" ]
];