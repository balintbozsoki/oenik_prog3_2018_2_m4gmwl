var hierarchy =
[
    [ "StockViewer.Data.battery", "class_stock_viewer_1_1_data_1_1battery.html", null ],
    [ "StockViewer.Data.brands", "class_stock_viewer_1_1_data_1_1brands.html", null ],
    [ "StockViewer.Data.builds", "class_stock_viewer_1_1_data_1_1builds.html", null ],
    [ "StockViewer.Logic.CallMethod", "class_stock_viewer_1_1_logic_1_1_call_method.html", null ],
    [ "StockViewer.Data.Cams", "class_stock_viewer_1_1_data_1_1_cams.html", null ],
    [ "OENIK_PROG3_2018_2_M4GMWL.Class1", "class_o_e_n_i_k___p_r_o_g3__2018__2___m4_g_m_w_l_1_1_class1.html", null ],
    [ "StockViewer.Logic.ComplexOperations", "class_stock_viewer_1_1_logic_1_1_complex_operations.html", null ],
    [ "StockViewer.Logic.CreateNewRecord", "class_stock_viewer_1_1_logic_1_1_create_new_record.html", null ],
    [ "DbContext", null, [
      [ "StockViewer.Data.MultirotorDBEntities", "class_stock_viewer_1_1_data_1_1_multirotor_d_b_entities.html", null ],
      [ "StockViewer.Data.TestMultirotorDBEntities", "class_stock_viewer_1_1_data_1_1_test_multirotor_d_b_entities.html", null ]
    ] ],
    [ "StockViewer.Program.DisplayMenuItems", "class_stock_viewer_1_1_program_1_1_display_menu_items.html", null ],
    [ "StockViewer.Data.ESC", "class_stock_viewer_1_1_data_1_1_e_s_c.html", null ],
    [ "StockViewer.Data.FC", "class_stock_viewer_1_1_data_1_1_f_c.html", null ],
    [ "StockViewer.Data.frames", "class_stock_viewer_1_1_data_1_1frames.html", null ],
    [ "StockViewer.Logic.GetTheJoke", "class_stock_viewer_1_1_logic_1_1_get_the_joke.html", null ],
    [ "IDisposable", null, [
      [ "StockViewer.Logic.Operations< T >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
      [ "StockViewer.Logic.QueryAllTables", "class_stock_viewer_1_1_logic_1_1_query_all_tables.html", null ],
      [ "StockViewer.Logic.Test.OperationsTests", "class_stock_viewer_1_1_logic_1_1_test_1_1_operations_tests.html", null ]
    ] ],
    [ "StockViewer.Logic.Interfaces.ILogic< T >", "interface_stock_viewer_1_1_logic_1_1_interfaces_1_1_i_logic.html", [
      [ "StockViewer.Logic.Operations< T >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ]
    ] ],
    [ "StockViewer.Repository.Interfaces.IRepository< T >", "interface_stock_viewer_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "StockViewer.Repository.SqlRepository< T >", "class_stock_viewer_1_1_repository_1_1_sql_repository.html", null ]
    ] ],
    [ "StockViewer.Repository.Tests.MockDbSetFactory", "class_stock_viewer_1_1_repository_1_1_tests_1_1_mock_db_set_factory.html", null ],
    [ "StockViewer.Logic.Tests.MockDbSetFactory", "class_stock_viewer_1_1_logic_1_1_tests_1_1_mock_db_set_factory.html", null ],
    [ "StockViewer.Data.motors", "class_stock_viewer_1_1_data_1_1motors.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.battery >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.brands >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.builds >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.Cams >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.ESC >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.FC >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.frames >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.motors >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.PDB >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.prop >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Operations< StockViewer.Data.VTX >", "class_stock_viewer_1_1_logic_1_1_operations.html", null ],
    [ "StockViewer.Logic.Param", "class_stock_viewer_1_1_logic_1_1_param.html", null ],
    [ "StockViewer.Logic.ParamCollector", "class_stock_viewer_1_1_logic_1_1_param_collector.html", null ],
    [ "StockViewer.Program.ParamInputDialog", "class_stock_viewer_1_1_program_1_1_param_input_dialog.html", null ],
    [ "StockViewer.Data.PDB", "class_stock_viewer_1_1_data_1_1_p_d_b.html", null ],
    [ "StockViewer.Program.Program", "class_stock_viewer_1_1_program_1_1_program.html", null ],
    [ "StockViewer.Data.prop", "class_stock_viewer_1_1_data_1_1prop.html", null ],
    [ "StockViewer.Logic.PropertyExtractor", "class_stock_viewer_1_1_logic_1_1_property_extractor.html", null ],
    [ "StockViewer.Repository.SqlRepository< StockViewer.Data.frames >", "class_stock_viewer_1_1_repository_1_1_sql_repository.html", null ],
    [ "StockViewer.Repository.Tests.SqlRepositoryTests", "class_stock_viewer_1_1_repository_1_1_tests_1_1_sql_repository_tests.html", null ],
    [ "StockViewer.Data.VTX", "class_stock_viewer_1_1_data_1_1_v_t_x.html", null ],
    [ "StockViewer.Program.XmlHandler", "class_stock_viewer_1_1_program_1_1_xml_handler.html", null ]
];