var class_stock_viewer_1_1_logic_1_1_operations =
[
    [ "Operations", "class_stock_viewer_1_1_logic_1_1_operations.html#a367ef12e4f0ec670fa10c292f9c4cbc5", null ],
    [ "Delete", "class_stock_viewer_1_1_logic_1_1_operations.html#ab768208a1a4c7b7f2d03ad53276b27c9", null ],
    [ "Dispose", "class_stock_viewer_1_1_logic_1_1_operations.html#aaddd24bf3f89199906e7339fe33fdc98", null ],
    [ "Dispose", "class_stock_viewer_1_1_logic_1_1_operations.html#a0ad27233d88458d988232dcd9cf74a46", null ],
    [ "Find", "class_stock_viewer_1_1_logic_1_1_operations.html#aa1dde35a12f89ce63bf0137d1c617e28", null ],
    [ "Insert", "class_stock_viewer_1_1_logic_1_1_operations.html#a72a136175a3aa0dd868d8abfed2804d0", null ],
    [ "Update", "class_stock_viewer_1_1_logic_1_1_operations.html#a2518299b218d1fddc006c37a71956474", null ],
    [ "Result", "class_stock_viewer_1_1_logic_1_1_operations.html#ac06b2bd7ea39e3a97686ff2a34ff0c50", null ]
];