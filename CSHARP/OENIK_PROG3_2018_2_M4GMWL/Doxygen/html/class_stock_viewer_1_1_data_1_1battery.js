var class_stock_viewer_1_1_data_1_1battery =
[
    [ "battery", "class_stock_viewer_1_1_data_1_1battery.html#aed9efc336b07baa1fd7b341077ae2763", null ],
    [ "brand_id", "class_stock_viewer_1_1_data_1_1battery.html#af404fe758a68adddc0ee4126bd288d32", null ],
    [ "brands", "class_stock_viewer_1_1_data_1_1battery.html#aac9e8e9edc84a9709e4d1fd4e3b9c630", null ],
    [ "builds", "class_stock_viewer_1_1_data_1_1battery.html#a71f1569874620a47390506253a1ebb78", null ],
    [ "c", "class_stock_viewer_1_1_data_1_1battery.html#a2feeec3b3bd43f95419c625866641fe0", null ],
    [ "cells", "class_stock_viewer_1_1_data_1_1battery.html#a186c90343117e31dca22c3a82bf822b4", null ],
    [ "id", "class_stock_viewer_1_1_data_1_1battery.html#aed3cf2ab5aee24700fcf69c411eaad07", null ],
    [ "mass", "class_stock_viewer_1_1_data_1_1battery.html#aad81a64d81293f404dd3be595a770aac", null ],
    [ "name", "class_stock_viewer_1_1_data_1_1battery.html#ac43cbbc0e225b0cc9e391c4b48724efe", null ]
];