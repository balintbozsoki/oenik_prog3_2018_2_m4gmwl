var class_stock_viewer_1_1_data_1_1motors =
[
    [ "motors", "class_stock_viewer_1_1_data_1_1motors.html#a8491cec0a67e48f41e0ebe81650cbdb8", null ],
    [ "ampdraw", "class_stock_viewer_1_1_data_1_1motors.html#a653aa05f31fced13936e204a5f0dff0c", null ],
    [ "brand_id", "class_stock_viewer_1_1_data_1_1motors.html#a96c2ab229acf42aa59b11773dbbb4551", null ],
    [ "brands", "class_stock_viewer_1_1_data_1_1motors.html#a113ba104bc334c2938a10960139f41fb", null ],
    [ "builds", "class_stock_viewer_1_1_data_1_1motors.html#ab324d4a0ff893ec811c3b57396aa736c", null ],
    [ "cells", "class_stock_viewer_1_1_data_1_1motors.html#ac669b86fbb4d00989e641872d9e908b6", null ],
    [ "id", "class_stock_viewer_1_1_data_1_1motors.html#ab9c4d3b6404a42d890724435d116ffab", null ],
    [ "kv", "class_stock_viewer_1_1_data_1_1motors.html#a437dea0e6af51bb51a3bceb808b4bf72", null ],
    [ "mass", "class_stock_viewer_1_1_data_1_1motors.html#a09cd62057bf9e8a371861bbcc91546eb", null ],
    [ "name", "class_stock_viewer_1_1_data_1_1motors.html#a8fa13eb4ffd9eb393038fc3839f865d0", null ],
    [ "size", "class_stock_viewer_1_1_data_1_1motors.html#a5af0189c9d7636d3583e2faa4acc1664", null ],
    [ "thrust", "class_stock_viewer_1_1_data_1_1motors.html#a41c79f3f90826fc24fc69f55f50f52da", null ]
];