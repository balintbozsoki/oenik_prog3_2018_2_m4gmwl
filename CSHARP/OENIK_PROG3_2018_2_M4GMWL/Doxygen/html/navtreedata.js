/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "OENIK_PROG3_2018_2_M4GMWL", "index.html", [
    [ "Castle Core Changelog", "md__c_1__users__b_xC3_xA1lint__documents_oenik_prog3_2018_2_m4gmwl__c_s_h_a_r_p__o_e_n_i_k__p_r_4fe46c2228db56db6ce91baae9fa02f0.html", null ],
    [ "LICENSE", "md__c_1__users__b_xC3_xA1lint__documents_oenik_prog3_2018_2_m4gmwl__c_s_h_a_r_p__o_e_n_i_k__p_r_3e85b45a1df02aadbf3c267f3427d77b.html", null ],
    [ "NUnit 3.11 - October 11, 2018", "md__c_1__users__b_xC3_xA1lint__documents_oenik_prog3_2018_2_m4gmwl__c_s_h_a_r_p__o_e_n_i_k__p_r_6a7ed272ccfd37b1171925eaaf7ffce3.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_call_method_8cs_source.html",
"class_stock_viewer_1_1_data_1_1prop.html#ac67fc661b2d973949910cd989a352904"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';