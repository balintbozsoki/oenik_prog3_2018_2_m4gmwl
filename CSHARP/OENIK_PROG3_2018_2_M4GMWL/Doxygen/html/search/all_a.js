var searchData=
[
  ['nunit_203_2e11_20_2d_20october_2011_2c_202018',['NUnit 3.11 - October 11, 2018',['../md__c_1__users__b_xC3_xA1lint__documents_oenik_prog3_2018_2_m4gmwl__c_s_h_a_r_p__o_e_n_i_k__p_r_6a7ed272ccfd37b1171925eaaf7ffce3.html',1,'']]],
  ['name',['Name',['../class_stock_viewer_1_1_logic_1_1_param.html#a9b5e9c59fd1aedb470a8caf2b47a1c69',1,'StockViewer::Logic::Param']]],
  ['newbattery',['NewBattery',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a3ff90344f3d7d5c75770989178f984c0',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newbrand',['NewBrand',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a187bc84dd37a4076c18c401bbee6309c',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newbuild',['NewBuild',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a116392d11e2bba5dc79d57b7a7b6a603',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newcam',['NewCam',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a3a410d63dfdd4e9c4b7ce6f4821a32b4',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newesc',['NewEsc',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a0eb91d07c3195d63e1859b13e5a46589',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newfc',['NewFC',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a5457284302a82e342613698f14095acf',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newframe',['NewFrame',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a9f1a3e476f8d4ffe3182e2dc9ffd3382',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newmotor',['Newmotor',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a1c6f0c401b47f65efaa6f28bba2b72a7',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newpdb',['NewPdb',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a306a8bea1f03d0e3bf57bb14680d192f',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newprop',['Newprop',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a020a38f43c75f51da95c020d0c287076',1,'StockViewer::Logic::CreateNewRecord']]],
  ['newrecords',['NewRecords',['../class_stock_viewer_1_1_logic_1_1_param_collector.html#a7f67853615be1b3951a7a1578fb5617e',1,'StockViewer::Logic::ParamCollector']]],
  ['newvtx',['NewVtx',['../class_stock_viewer_1_1_logic_1_1_create_new_record.html#a5ff269e03e45697b7fe7a65343e4d7ae',1,'StockViewer::Logic::CreateNewRecord']]]
];
