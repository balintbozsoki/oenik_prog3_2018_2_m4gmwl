/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Bálint
 */
public class GenerateJoke {
    ArrayList<String> joke = new ArrayList<String>();

    public GenerateJoke() 
    {
        joke.add("I bought a ceiling fan the other day. Complete waste of money. He just stands there applauding and saying: Ooh, I love how smooth it is.");
        joke.add("Why did the scarecrow win an award? He was outstanding in his field.");
        joke.add("What do you call a fish with no eye? Fsh!");
        joke.add("Why could not the bicycle stand up by itself? It was two tired.");
        joke.add("What do you call a man with a rubber toe? Roberto.");
        joke.add("What kind of shoes do ninjas wear? Sneakers.");
        joke.add("Did you hear about the kidnapping at school? It is fine, he woke up.");
        joke.add("The rotation of earth really makes my day.");
        joke.add("The shovel was a ground-breaking invention.");
        joke.add("When does a joke become a dad joke? When it becomes apparent!");
    }
    
    public Jokes SingleJoke()
    {
        Random rand = new Random();
        int n = rand.nextInt(joke.size());
        Jokes j = new Jokes(joke.get(n));
        return j;
    }
}
